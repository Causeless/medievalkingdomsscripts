--[[
    Copyright 2016, 2017 Tommy March

    This file is part of AE-Tools.

    AE-Tools is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    AE-Tools is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with AE-Tools.  If not, see <http://www.gnu.org/licenses/>.
--]]

-- lib_year_events.lua
-- Stores table holding list of yearly events

-- General
module(..., package.seeall)
_G.main_env = getfenv(1) -- Probably not needed in most places

local dev = require "lua_scripts.dev"
local ui_campaign = require "lua_scripts.ui_campaign"

function showHistoricalEvents(year)
    -- We dynamically create the message ID
    local modifiedMessageID = "event_feed_hist_events_strings_" .. year

    local titleMessageID = modifiedMessageID .. "_title"
    local primaryDetailMessageID = modifiedMessageID .. "_primary_detail"
    local secondaryDetailMessageID = modifiedMessageID .. "_secondary_detail"

    ui_campaign.showMessageToHumans(titleMessageID, primaryDetailMessageID, secondaryDetailMessageID)
end

-- You can have higher fidelity by adding to the turn year; for example, (-218 + 1/4), which is good for >1 turn per year (in this case, 4tpy)
-- Events years are rounded up to the turn, so if you have 1 tpy and set an event for half way through a year, the event will appear the turn after. 

-- The key of the table is the year, and the second element is a table of functions

-- Note that if you add showHistoricalEvents to a year without any, a blank event will be shown
-- Also, currently this system cannot handle more than 1 event message a turn

-- Also, this needs to be fixed to support different TPYs generically
-- Right now if you have a 2TPY and have an event as 395.5, for example, then it's work correctly with 4TPY
-- But if you have 8TPY with an event at 395.25, then the 2TPY campaign would skip it
-- It's not a huge deal, but still to be thought about
events = {
    [395] = {showHistoricalEvents},
    [396] = {showHistoricalEvents},
    [397] = {showHistoricalEvents} -- There is no event defined for this in the DB
}