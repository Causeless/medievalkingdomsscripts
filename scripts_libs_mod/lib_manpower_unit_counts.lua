--[[
    Copyright 2016, 2017 Tommy March

    This file is part of AE-Tools.

    AE-Tools is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    AE-Tools is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with AE-Tools.  If not, see <http://www.gnu.org/licenses/>.
--]]

-- lib_manpower_unit_counts.lua
-- Stores table holding unit sizes

-- This is auto-generated right now. manual changes may be overwritten!

-- General
module(..., package.seeall)
_G.main_env = getfenv(1) -- Probably not needed in most places

units = {
	["AlmushatAlthaqilah"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["Bahadir"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["Baryn-YaramSpearmen"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["Baryn-YaramSwordsmen"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["BekhHorseArchers"] = {
		culture = "other",
		class = "rich",
		count = 80
	},

	["BekhLancers"] = {
		culture = "other",
		class = "rich",
		count = 80
	},

	["BirigHorseArchers"] = {
		culture = "other",
		class = "med",
		count = 80
	},

	["BirigSpearmen"] = {
		culture = "other",
		class = "poor",
		count = 180
	},

	["BirigSwordsmen"] = {
		culture = "other",
		class = "poor",
		count = 180
	},

	["BolokhovianPlunderers"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["CamelArchers"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["CumanHeavyRaiders"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["CumanHunter"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["CumanRaiders"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["DismountedAskaris"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["DismountedBekhs"] = {
		culture = "other",
		class = "med",
		count = 160
	},

	["Emir'sGuards"] = {
		culture = "other",
		class = "med",
		count = 50
	},

	["FursanAlnashab"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["GuzarCavalry"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["GuzarCrossbows"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["HeavyMacemen"] = {
		culture = "other",
		class = "med",
		count = 160
	},

	["KhaganGuardsmen"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["KipchakSwordsmen"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["MK_AOR_nav_chelandion_melee_tzakones"] = {
		culture = "other",
		class = "med",
		count = 160
	},

	["MK_AOR_nav_chelandion_missile_tzakones"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["MK_byz_ballista"] = {
		culture = "other",
		class = "poor",
		count = 40
	},

	["MK_byz_bombard"] = {
		culture = "other",
		class = "rich",
		count = 50
	},

	["MK_byz_catapult"] = {
		culture = "other",
		class = "poor",
		count = 40
	},

	["MK_byz_fire_siphon_bastion"] = {
		culture = "other",
		class = "poor",
		count = 4
	},

	["MK_byz_great_bombard"] = {
		culture = "other",
		class = "med",
		count = 30
	},

	["MK_byz_mortar"] = {
		culture = "other",
		class = "med",
		count = 40
	},

	["MK_byz_nav_chelandion_catapult"] = {
		culture = "other",
		class = "med",
		count = 60
	},

	["MK_byz_nav_chelandion_melee_droungarios"] = {
		culture = "other",
		class = "med",
		count = 160
	},

	["MK_byz_nav_chelandion_melee_pelekyphoroi"] = {
		culture = "other",
		class = "rich",
		count = 160
	},

	["MK_byz_nav_chelandion_melee_trebizond"] = {
		culture = "other",
		class = "med",
		count = 160
	},

	["MK_byz_nav_chelandion_melee_tzakones"] = {
		culture = "other",
		class = "med",
		count = 160
	},

	["MK_byz_nav_chelandion_missile_gasmouloi"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["MK_byz_nav_chelandion_missile_trebizond"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["MK_byz_nav_chelandion_missile_tzakones"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["MK_byz_nav_chelandion_siphon"] = {
		culture = "other",
		class = "rich",
		count = 100
	},

	["MK_byz_nav_dromonarion_melee_gasmouloi"] = {
		culture = "other",
		class = "poor",
		count = 90
	},

	["MK_byz_nav_dromonarion_melee_prosalentai"] = {
		culture = "other",
		class = "poor",
		count = 90
	},

	["MK_byz_nav_dromonarion_missile_gasmouloi"] = {
		culture = "other",
		class = "poor",
		count = 90
	},

	["MK_byz_nav_dromonarion_missile_prosalentai"] = {
		culture = "other",
		class = "poor",
		count = 90
	},

	["MK_byz_nav_dromonarion_missile_trebizond"] = {
		culture = "other",
		class = "poor",
		count = 90
	},

	["MK_byz_nav_dromonarion_missile_tzakones"] = {
		culture = "other",
		class = "poor",
		count = 90
	},

	["MK_byz_trebuchet"] = {
		culture = "other",
		class = "poor",
		count = 50
	},

	["MK_eas_ballista_bastion"] = {
		culture = "other",
		class = "poor",
		count = 4
	},

	["MK_eas_cannon_bastion"] = {
		culture = "other",
		class = "poor",
		count = 4
	},

	["MK_eas_catapult_bastion"] = {
		culture = "other",
		class = "poor",
		count = 4
	},

	["MK_eur_ballista"] = {
		culture = "other",
		class = "poor",
		count = 40
	},

	["MK_eur_ballista_bastion"] = {
		culture = "other",
		class = "poor",
		count = 4
	},

	["MK_eur_bombard"] = {
		culture = "other",
		class = "rich",
		count = 50
	},

	["MK_eur_cannon_bastion"] = {
		culture = "other",
		class = "poor",
		count = 4
	},

	["MK_eur_catapult"] = {
		culture = "other",
		class = "poor",
		count = 40
	},

	["MK_eur_catapult_bastion"] = {
		culture = "other",
		class = "poor",
		count = 4
	},

	["MK_eur_great_bombard"] = {
		culture = "other",
		class = "med",
		count = 30
	},

	["MK_eur_mortar"] = {
		culture = "other",
		class = "med",
		count = 40
	},

	["MK_eur_trebuchet"] = {
		culture = "other",
		class = "poor",
		count = 50
	},

	["MK_merc_nav_dromonarion_melee_italian"] = {
		culture = "other",
		class = "med",
		count = 90
	},

	["MK_merc_nav_dromonarion_missile_italian"] = {
		culture = "other",
		class = "med",
		count = 90
	},

	["MK_mon_ballista"] = {
		culture = "other",
		class = "poor",
		count = 40
	},

	["MK_mon_ballista_bastion"] = {
		culture = "other",
		class = "poor",
		count = 4
	},

	["MK_mon_bombard"] = {
		culture = "other",
		class = "rich",
		count = 50
	},

	["MK_mon_cannon_bastion"] = {
		culture = "other",
		class = "poor",
		count = 4
	},

	["MK_mon_catapult"] = {
		culture = "other",
		class = "poor",
		count = 40
	},

	["MK_mon_catapult_bastion"] = {
		culture = "other",
		class = "poor",
		count = 4
	},

	["MK_mon_mortar"] = {
		culture = "other",
		class = "med",
		count = 40
	},

	["MK_mon_rocket"] = {
		culture = "other",
		class = "med",
		count = 40
	},

	["MK_mon_trebuchet"] = {
		culture = "other",
		class = "poor",
		count = 50
	},

	["MK_mus_ballista"] = {
		culture = "other",
		class = "poor",
		count = 40
	},

	["MK_mus_bombard"] = {
		culture = "other",
		class = "rich",
		count = 50
	},

	["MK_mus_catapult"] = {
		culture = "other",
		class = "poor",
		count = 40
	},

	["MK_mus_great_bombard"] = {
		culture = "other",
		class = "med",
		count = 30
	},

	["MK_mus_mortar"] = {
		culture = "other",
		class = "med",
		count = 40
	},

	["MK_mus_trebuchet"] = {
		culture = "other",
		class = "poor",
		count = 50
	},

	["PeasentArchers"] = {
		culture = "other",
		class = "poor",
		count = 180
	},

	["PechenegJavelinmen"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["Rus_Malaia"] = {
		culture = "other",
		class = "rich",
		count = 80
	},

	["SlavicAxemen"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["SteppeHandgunners"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["Tawashi"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["TerterobaCavalry"] = {
		culture = "other",
		class = "rich",
		count = 80
	},

	["VenetianArcher"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["VenetianBerrovieri"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["VenetianCrossbowmen"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["VenetianCrossbowmen(High)"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["VenetianFootKnight"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["VenetianGeneralsUnit"] = {
		culture = "other",
		class = "med",
		count = 50
	},

	["VenetianGeneralsUnit(High)"] = {
		culture = "other",
		class = "med",
		count = 50
	},

	["VenetianInfantry(High)"] = {
		culture = "other",
		class = "med",
		count = 160
	},

	["VenetianKnight"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["VenetianKnights(High)"] = {
		culture = "other",
		class = "rich",
		count = 80
	},

	["VenetianKnightsDsimounted(High)"] = {
		culture = "other",
		class = "rich",
		count = 160
	},

	["VenetianMilita"] = {
		culture = "other",
		class = "poor",
		count = 180
	},

	["VenetianMilitaCrossbowmen"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["VenetianMountedBerrovieri"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["VenetianMountedCrossbowmen"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["VenetianMountedCrossbowmen(High)"] = {
		culture = "other",
		class = "med",
		count = 80
	},

	["VenetianPikemen"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["VenetianPikemen(High)"] = {
		culture = "other",
		class = "med",
		count = 160
	},

	["VenetianScout"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["VenetianSpearman"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["VenetianSpearman(High)"] = {
		culture = "other",
		class = "med",
		count = 160
	},

	["VenetianSwordsman(Early)"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["VenetianSwordsman(High)"] = {
		culture = "other",
		class = "med",
		count = 160
	},

	["VlachHeavyArchers"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["ahones_retinue"] = {
		culture = "other",
		class = "med",
		count = 50
	},

	["alagon_retinue"] = {
		culture = "other",
		class = "med",
		count = 50
	},

	["almo_late_crossbowmen"] = {
		culture = "other",
		class = "rich",
		count = 120
	},

	["almo_late_handgunners"] = {
		culture = "other",
		class = "rich",
		count = 120
	},

	["almo_late_infantry"] = {
		culture = "other",
		class = "rich",
		count = 160
	},

	["almocaden"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["amp_retinuie"] = {
		culture = "other",
		class = "med",
		count = 50
	},

	["angle_retinue"] = {
		culture = "other",
		class = "med",
		count = 50
	},

	["anjou_castellans"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["anjou_crossbowmen"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["anjou_maa"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["anjou_mounted_maa"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["anjou_mounted_serjeants"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["anjou_retinue"] = {
		culture = "other",
		class = "med",
		count = 50
	},

	["anjou_spearmen"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["anjou_voulgier"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["aragon_crossbowmen"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["aragon_knights"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["aragon_late_knights"] = {
		culture = "other",
		class = "rich",
		count = 80
	},

	["aragon_late_retinue"] = {
		culture = "other",
		class = "rich",
		count = 50
	},

	["aragon_mid_knights"] = {
		culture = "other",
		class = "med",
		count = 80
	},

	["aragon_mounted_crossbowmen"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["aragon_mounted_crossbowmen_high"] = {
		culture = "other",
		class = "med",
		count = 80
	},

	["aragon_mounted_crossbowmen_late"] = {
		culture = "other",
		class = "med",
		count = 80
	},

	["aragon_nobles"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["aragon_pikemen"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["aragon_retinue"] = {
		culture = "other",
		class = "med",
		count = 50
	},

	["aragon_swordsmen"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["archers"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["archersergeants"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["arg_crossbow_militia"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["arg_heavy_crossbowmen"] = {
		culture = "other",
		class = "med",
		count = 120
	},

	["arg_heavy_infantry"] = {
		culture = "other",
		class = "med",
		count = 160
	},

	["arg_heavy_serjeants"] = {
		culture = "other",
		class = "med",
		count = 160
	},

	["arg_heavy_spearmen"] = {
		culture = "other",
		class = "med",
		count = 160
	},

	["arg_late_infantry"] = {
		culture = "other",
		class = "rich",
		count = 160
	},

	["arg_maa"] = {
		culture = "other",
		class = "rich",
		count = 160
	},

	["arg_mid_mounted_serjeants"] = {
		culture = "other",
		class = "rich",
		count = 80
	},

	["arg_spear_militia"] = {
		culture = "other",
		class = "poor",
		count = 180
	},

	["armag_retinue"] = {
		culture = "other",
		class = "med",
		count = 50
	},

	["armored_sergeants"] = {
		culture = "other",
		class = "med",
		count = 160
	},

	["arundel_archers"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["arundel_billmen"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["arundel_castellans"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["arundel_maa"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["arundel_mounted_maa"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["arundel_mounted_serjeants"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["arundel_retinue"] = {
		culture = "other",
		class = "med",
		count = 50
	},

	["att_alan_alani_cavalry_raiders"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["att_alan_alani_cavalry_warriors"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["att_alan_alani_chosen_raiders"] = {
		culture = "other",
		class = "rich",
		count = 80
	},

	["att_alan_alani_horse_archers"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["att_alan_alani_mounted_bows"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["att_alan_alani_mounted_sword_raiders"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["att_alan_alani_nobles_general"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["att_alan_alani_swordsmen"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_alan_alani_warriors"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_alan_anti_cav_light_tier_3"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["att_alan_anti_cav_light_tier_4"] = {
		culture = "other",
		class = "med",
		count = 80
	},

	["att_alan_onager"] = {
		culture = "other",
		class = "poor",
		count = 40
	},

	["att_alan_sarmatian_band"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_alan_sarmatian_cataphracts_infantry_destroyer_tier_5"] = {
		culture = "other",
		class = "rich",
		count = 80
	},

	["att_alan_sarmatian_shock_tier_1"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["att_alan_sarmatian_shock_tier_4"] = {
		culture = "other",
		class = "rich",
		count = 80
	},

	["att_alan_tank"] = {
		culture = "other",
		class = "rich",
		count = 80
	},

	["att_cel_black_blades"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_cel_caledonian_axemen"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_cel_cateran_brigade"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_cel_celtic_archers"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_cel_celtic_axe_band"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_cel_celtic_axe_warriors"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_cel_celtic_band"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_cel_celtic_berserkers"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_cel_celtic_bows"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_cel_celtic_brigands"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_cel_celtic_cavalry_raiders"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["att_cel_celtic_crossbows"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_cel_celtic_levy"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_cel_celtic_nobles"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_cel_celtic_skirmishers"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_cel_celtic_spearmen"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_cel_celtic_spears"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_cel_celtic_warband"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_cel_celtic_warlord"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_cel_celtic_warriors"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_cel_celtic_woodrunners"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_cel_chosen_raiders"] = {
		culture = "other",
		class = "med",
		count = 80
	},

	["att_cel_ebdani_cavalry_raiders"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["att_cel_ebdani_raiders"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_cel_elite_highland_archers"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_cel_fianna"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_cel_followers_of_morrigan"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_cel_gallowglass"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_cel_gazehounds"] = {
		culture = "other",
		class = "poor",
		count = 40
	},

	["att_cel_highland_archers"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_cel_horse_whisperers"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_cel_kerns"] = {
		culture = "other",
		class = "med",
		count = 160
	},

	["att_cel_kings_fianna"] = {
		culture = "other",
		class = "med",
		count = 80
	},

	["att_cel_kings_warband"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_cel_mormaer_cavalry"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["att_cel_mounted_spear_raiders"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["att_cel_noble_archers"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_cel_onager"] = {
		culture = "other",
		class = "poor",
		count = 40
	},

	["att_cel_onager_bastion"] = {
		culture = "other",
		class = "poor",
		count = 4
	},

	["att_cel_pictish_axemen"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_cel_pictish_berserkers"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_cel_pictish_swordsmen"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_cel_round_shield_raiders"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["att_cel_royal_archers"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_cel_scathas_teachers"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["att_cel_sighthound_spears"] = {
		culture = "other",
		class = "poor",
		count = 40
	},

	["att_cel_tanist"] = {
		culture = "other",
		class = "med",
		count = 160
	},

	["att_cel_wolfhound_spearmen"] = {
		culture = "other",
		class = "poor",
		count = 40
	},

	["att_des_amazigh_lancers"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["att_des_amazigh_raiders"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["att_des_amazigh_spearmen"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_des_arabic_horsemen"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["att_des_armoured_camel_warriors"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["att_des_camel_lancers"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["att_des_camel_warriors"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["att_des_desert_bowmen"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_des_desert_chieftain"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["att_des_desert_hurlers"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_des_desert_legionary_defectors"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_des_desert_levy"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_des_desert_mounted_javelinmen"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["att_des_desert_mounted_skirmishers"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["att_des_desert_palatina_defectors"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_des_desert_raiders"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["att_des_desert_spears"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_des_desert_tribesmen"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_des_desert_warlord"] = {
		culture = "other",
		class = "med",
		count = 80
	},

	["att_des_elite_nubian_archers"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_des_es_abunas_guard"] = {
		culture = "other",
		class = "med",
		count = 80
	},

	["att_des_es_adana_marksmen"] = {
		culture = "other",
		class = "poor",
		count = 40
	},

	["att_des_es_adana_trackers"] = {
		culture = "other",
		class = "poor",
		count = 40
	},

	["att_des_es_afar_camel_riders"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["att_des_es_afar_raidmasters"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_des_es_afar_swordsmen"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_des_es_al_dawser"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["att_des_es_al_rahrain"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_des_es_al_shahba"] = {
		culture = "other",
		class = "med",
		count = 80
	},

	["att_des_es_almaqahs_lancers"] = {
		culture = "other",
		class = "med",
		count = 80
	},

	["att_des_es_armoured_himyarite_shotelai"] = {
		culture = "other",
		class = "med",
		count = 80
	},

	["att_des_es_ashum"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_des_es_athars_chosen"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["att_des_es_badyia_skirmishers"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_des_es_baltha_defenders"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_des_es_baltha_warriors"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_des_es_behers_chosen"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_des_es_bet_giorgis_cavalry"] = {
		culture = "other",
		class = "med",
		count = 80
	},

	["att_des_es_desert_pikes"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_des_es_dune_lancers"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["att_des_es_elite_tor_warriors"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_des_es_himyarite_shotelai"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["att_des_es_jamal_al_baltha"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["att_des_es_jamal_al_rumha"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["att_des_es_khahyahlim"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_des_es_kings_radif"] = {
		culture = "other",
		class = "med",
		count = 80
	},

	["att_des_es_marz_archers"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_des_es_masqal_spearmen"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_des_es_mavias_bodyguards"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["att_des_es_mavias_chargers"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["att_des_es_mavias_chosen"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["att_des_es_mavias_lancers"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["att_des_es_mounted_marz_archers"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["att_des_es_noble_al_rahrain"] = {
		culture = "other",
		class = "med",
		count = 160
	},

	["att_des_es_ras_guard"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_des_es_rebellion_militia"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_des_es_rumha_skirmishers"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_des_es_rumha_warriors"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_des_es_sahnegohrim"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_des_es_sanai"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_des_es_sandstorm_lancers"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["att_des_es_sons_of_the_invincible_mahrem"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_des_es_spice_guard"] = {
		culture = "other",
		class = "med",
		count = 160
	},

	["att_des_es_spice_warriors"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_des_es_tanukhid_ambushers"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_des_es_tanukhid_pikes"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_des_es_tor_warriors"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_des_es_wadai"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["att_des_es_zafar_sentinels"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_des_es_zealot_sicarius"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_des_es_zodiac_archers"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["att_des_garamantian_hunters"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_des_garamantian_lancers"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["att_des_kushite_mounted_shotelai"] = {
		culture = "other",
		class = "med",
		count = 80
	},

	["att_des_kushite_shotelai"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_des_lancers_of_the_oasis"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["att_des_noble_numidian_cavalry"] = {
		culture = "other",
		class = "rich",
		count = 80
	},

	["att_des_onager"] = {
		culture = "other",
		class = "poor",
		count = 40
	},

	["att_des_onager_bastion"] = {
		culture = "other",
		class = "poor",
		count = 4
	},

	["att_des_shawia_guard"] = {
		culture = "other",
		class = "med",
		count = 160
	},

	["att_des_tuareg_camel_spearmen"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["att_est_armenian_slingers"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_est_armenian_spears"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_est_armoured_indian_elephants"] = {
		culture = "other",
		class = "rich",
		count = 24
	},

	["att_est_camel_clibanarii"] = {
		culture = "other",
		class = "med",
		count = 80
	},

	["att_est_civilian_farmers"] = {
		culture = "other",
		class = "poor",
		count = 40
	},

	["att_est_civilian_townsfolk"] = {
		culture = "other",
		class = "poor",
		count = 40
	},

	["att_est_dailamite_warriors"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_est_elite_dailamite_infantry"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_est_elite_immortals"] = {
		culture = "other",
		class = "med",
		count = 160
	},

	["att_est_elite_savaran_cavalry"] = {
		culture = "other",
		class = "med",
		count = 80
	},

	["att_est_grivpanvar_cataphracts"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["att_est_gyan_avspar"] = {
		culture = "other",
		class = "rich",
		count = 80
	},

	["att_est_immortals"] = {
		culture = "other",
		class = "med",
		count = 160
	},

	["att_est_indian_elephants"] = {
		culture = "other",
		class = "rich",
		count = 24
	},

	["att_est_kurdish_archers"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_est_kurdish_javelinmen"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_est_lakhmid_scouts"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["att_est_naft_throwers"] = {
		culture = "other",
		class = "poor",
		count = 40
	},

	["att_est_onager"] = {
		culture = "other",
		class = "poor",
		count = 40
	},

	["att_est_onager_bastion"] = {
		culture = "other",
		class = "poor",
		count = 4
	},

	["att_est_onager_large"] = {
		culture = "other",
		class = "rich",
		count = 40
	},

	["att_est_paighan_band"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_est_persian_bowmen"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_est_persian_bows"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_est_persian_brigade"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_est_persian_camel_archers"] = {
		culture = "other",
		class = "med",
		count = 80
	},

	["att_est_persian_camel_raiders"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["att_est_persian_cataphracts"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["att_est_persian_hurlers"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_est_persian_levy"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_est_persian_mounted_bowmen"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["att_est_persian_mounted_warriors"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["att_est_persian_nobles"] = {
		culture = "other",
		class = "rich",
		count = 80
	},

	["att_est_persian_scouts"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["att_est_persian_skirmishers"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_est_pushtighban_cataphracts"] = {
		culture = "other",
		class = "med",
		count = 80
	},

	["att_est_royal_persian_archers"] = {
		culture = "other",
		class = "med",
		count = 160
	},

	["att_est_sassanid_onager_large"] = {
		culture = "other",
		class = "rich",
		count = 40
	},

	["att_est_savaran_cataphracts"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["att_est_savaran_cavalry"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["att_est_savaran_sardar"] = {
		culture = "other",
		class = "rich",
		count = 80
	},

	["att_est_sogdian_camel_raiders"] = {
		culture = "other",
		class = "med",
		count = 80
	},

	["att_est_sogdian_warriors"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_est_spahbed"] = {
		culture = "other",
		class = "rich",
		count = 80
	},

	["att_est_zhayedan_immortal_cavalry"] = {
		culture = "other",
		class = "med",
		count = 80
	},

	["att_ger_agathyrsi_warriors"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_ger_alamannic_scavengers"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_ger_alani_general"] = {
		culture = "other",
		class = "rich",
		count = 80
	},

	["att_ger_alani_horsemen"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["att_ger_alani_spearmen"] = {
		culture = "other",
		class = "poor",
		count = 40
	},

	["att_ger_antrustriones_cavalry_guard"] = {
		culture = "other",
		class = "med",
		count = 80
	},

	["att_ger_axe_heerbann"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_ger_bagaudae"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_ger_barbed_javelinmen"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_ger_barbed_lancers"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["att_ger_barbed_skirmishers"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_ger_bejewelled_retinue"] = {
		culture = "other",
		class = "med",
		count = 160
	},

	["att_ger_burgundian_axemen"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_ger_burgundian_guard"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_ger_burgundian_mounted_axemen"] = {
		culture = "other",
		class = "med",
		count = 80
	},

	["att_ger_burgundian_warhounds"] = {
		culture = "other",
		class = "poor",
		count = 40
	},

	["att_ger_captured_cheiroballistra"] = {
		culture = "other",
		class = "poor",
		count = 40
	},

	["att_ger_chnodomar's_entourage"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["att_ger_chnodomar's_raiders"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["att_ger_chosen_vandal_raiders"] = {
		culture = "other",
		class = "med",
		count = 80
	},

	["att_ger_civilian_farmers"] = {
		culture = "other",
		class = "poor",
		count = 40
	},

	["att_ger_civilian_townsfolk"] = {
		culture = "other",
		class = "poor",
		count = 40
	},

	["att_ger_elite_agathyrsi_cavalry"] = {
		culture = "other",
		class = "med",
		count = 80
	},

	["att_ger_elite_agathyrsi_warriors"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_ger_elite_alamannic_scavengers"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_ger_elite_germanic_archers"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_ger_elite_ostrogothic_pikemen"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_ger_elite_sarmatian_cataphracts"] = {
		culture = "other",
		class = "med",
		count = 80
	},

	["att_ger_elite_scattershot_slingers"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_ger_elite_sword_heerbann"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_ger_francisca_heerbann"] = {
		culture = "other",
		class = "med",
		count = 160
	},

	["att_ger_frankish_general"] = {
		culture = "other",
		class = "rich",
		count = 80
	},

	["att_ger_frankish_lancers"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["att_ger_frankish_skirmishers"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_ger_freemen"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_ger_gardingi_cavalry"] = {
		culture = "other",
		class = "med",
		count = 80
	},

	["att_ger_germanic_archers"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_ger_germanic_band"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_ger_germanic_bowmen"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_ger_germanic_bows"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_ger_germanic_brigands"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_ger_germanic_crossbowmen"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_ger_germanic_horsemen"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["att_ger_germanic_hunters"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_ger_germanic_hurlers"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_ger_germanic_lancers"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["att_ger_germanic_levy"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_ger_germanic_mounted_brigands"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["att_ger_germanic_mounted_warband"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["att_ger_germanic_night_raiders"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["att_ger_germanic_nobles"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_ger_germanic_pikes"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_ger_germanic_raiders"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["att_ger_germanic_spear_masters"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_ger_germanic_spearmen"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_ger_germanic_warband"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_ger_godan's_chosen"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_ger_godan's_warlord"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_ger_godansmen"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_ger_gothic_archers"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_ger_gothic_falxmen"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_ger_gothic_general"] = {
		culture = "other",
		class = "med",
		count = 80
	},

	["att_ger_gothic_lancers"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["att_ger_gothic_levy"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_ger_gothic_palatina_defectors"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_ger_gothic_pikemen"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_ger_gothic_warband"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_ger_grey_hairs"] = {
		culture = "other",
		class = "med",
		count = 80
	},

	["att_ger_horse_slayers"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_ger_horse_stabbers"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_ger_jewelled_nobles"] = {
		culture = "other",
		class = "med",
		count = 160
	},

	["att_ger_langobard_clubmen"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_ger_light_moorish_cavalry"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["att_ger_noble_alani_cavalry"] = {
		culture = "other",
		class = "med",
		count = 80
	},

	["att_ger_noble_germanic_horsemen"] = {
		culture = "other",
		class = "rich",
		count = 80
	},

	["att_ger_noble_germanic_swordsmen"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_ger_noble_gothic_lancers"] = {
		culture = "other",
		class = "rich",
		count = 80
	},

	["att_ger_noble_suebi_swordsmen"] = {
		culture = "other",
		class = "rich",
		count = 160
	},

	["att_ger_onager"] = {
		culture = "other",
		class = "poor",
		count = 40
	},

	["att_ger_onager_bastion"] = {
		culture = "other",
		class = "poor",
		count = 4
	},

	["att_ger_onager_large"] = {
		culture = "other",
		class = "rich",
		count = 40
	},

	["att_ger_protectores_defectors"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_ger_redoubtable_chieftain"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_ger_royal_anstrutiones"] = {
		culture = "other",
		class = "med",
		count = 160
	},

	["att_ger_royal_burgundian_general"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["att_ger_royal_lancers"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["att_ger_royal_saiones"] = {
		culture = "other",
		class = "rich",
		count = 80
	},

	["att_ger_royal_suebi_cavalry"] = {
		culture = "other",
		class = "med",
		count = 80
	},

	["att_ger_sacra_francisca"] = {
		culture = "other",
		class = "med",
		count = 80
	},

	["att_ger_salian_frankish_lancers"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["att_ger_sarmatian_cataphract_archers"] = {
		culture = "other",
		class = "rich",
		count = 80
	},

	["att_ger_sarmatian_cataphracts"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["att_ger_sarmatian_mounted_skirmishers"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["att_ger_sarmatian_spears"] = {
		culture = "other",
		class = "poor",
		count = 40
	},

	["att_ger_sarmatian_warband"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_ger_scaled_clubmen"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_ger_scattershot_hurlers"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_ger_scavenger_mob"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_ger_suebi_champions"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_ger_suebi_nobles"] = {
		culture = "other",
		class = "med",
		count = 80
	},

	["att_ger_suebi_oath_takers"] = {
		culture = "other",
		class = "med",
		count = 160
	},

	["att_ger_suebi_warriors"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_ger_sword_heerbann"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_ger_taifali_cavalry"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["att_ger_thracian_oathsworns"] = {
		culture = "other",
		class = "med",
		count = 160
	},

	["att_ger_thracian_warriors"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_ger_vandal_berserkers"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_ger_vandal_raiders"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["att_ger_vandal_warlord"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["att_ger_visigothic_slingers"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_ger_young_wolves"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_mar_ger_elite_vandal_archer_marauders"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["att_mar_rom_byzantine_light_marines"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["att_mar_rom_byzantine_marines"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["att_merc_cel_celtic_archers"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_merc_cel_celtic_axe_band"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_merc_cel_celtic_band"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_merc_cel_celtic_berserkers"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_merc_cel_celtic_brigands"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_merc_cel_celtic_cavalry_raiders"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["att_merc_cel_celtic_levy"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_merc_cel_celtic_skirmishers"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_merc_cel_celtic_spears"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_merc_cel_celtic_warband"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_merc_cel_gazehounds"] = {
		culture = "other",
		class = "poor",
		count = 40
	},

	["att_merc_cel_mounted_spear_raiders"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["att_merc_cel_onager"] = {
		culture = "other",
		class = "poor",
		count = 40
	},

	["att_merc_des_camel_warriors"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["att_merc_des_desert_bowmen"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_merc_des_desert_hurlers"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_merc_des_desert_legionary_defectors"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_merc_des_desert_raiders"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["att_merc_des_desert_spears"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_merc_des_elite_nubian_archers"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_merc_des_garamantian_lancers"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["att_merc_des_onager"] = {
		culture = "other",
		class = "rich",
		count = 40
	},

	["att_merc_des_tuareg_camel_spearmen"] = {
		culture = "other",
		class = "med",
		count = 80
	},

	["att_merc_est_armenian_slingers"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_merc_est_armenian_spears"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_merc_est_dailamite_warriors"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_merc_est_kurdish_archers"] = {
		culture = "other",
		class = "med",
		count = 160
	},

	["att_merc_est_kurdish_javelinmen"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_merc_est_lakhmid_scouts"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["att_merc_est_onager"] = {
		culture = "other",
		class = "rich",
		count = 40
	},

	["att_merc_est_persian_brigade"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_merc_est_persian_hurlers"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_merc_est_persian_mounted_bowmen"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["att_merc_est_persian_mounted_warriors"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["att_merc_est_persian_scouts"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["att_merc_est_persian_skirmishers"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_merc_est_savaran_cataphracts"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["att_merc_est_savaran_cavalry"] = {
		culture = "other",
		class = "med",
		count = 80
	},

	["att_merc_ger_agathyrsi_warriors"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_merc_ger_alani_horsemen"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["att_merc_ger_axe_heerbann"] = {
		culture = "other",
		class = "med",
		count = 160
	},

	["att_merc_ger_frankish_lancers"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["att_merc_ger_germanic_hunters"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_merc_ger_germanic_hurlers"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_merc_ger_germanic_mounted_brigands"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["att_merc_ger_germanic_mounted_warband"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["att_merc_ger_germanic_pikes"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_merc_ger_germanic_warband"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_merc_ger_gothic_falxmen"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_merc_ger_gothic_pikemen"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_merc_ger_light_moorish_cavalry"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["att_merc_ger_onager"] = {
		culture = "other",
		class = "rich",
		count = 40
	},

	["att_merc_ger_sarmatian_cataphracts"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["att_merc_ger_thracian_warriors"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_merc_ger_vandal_raiders"] = {
		culture = "other",
		class = "med",
		count = 80
	},

	["att_merc_nom_bosphoran_warriors"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_merc_nom_hunnic_horse_archers"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["att_merc_nom_hunnic_mounted_warband"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["att_merc_nom_onager"] = {
		culture = "other",
		class = "rich",
		count = 40
	},

	["att_merc_nom_steppe_lancers"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["att_merc_nom_steppe_mounted_bows"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["att_merc_nom_steppe_raiders"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["att_merc_nor_nordic_axe_warband"] = {
		culture = "other",
		class = "med",
		count = 160
	},

	["att_merc_nor_nordic_brigade"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_merc_nor_nordic_mounted_raiders"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["att_merc_nor_nordic_skirmishers"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_merc_nor_onager"] = {
		culture = "other",
		class = "rich",
		count = 40
	},

	["att_merc_nor_saxon_lancers"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["att_merc_nor_saxon_spears"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_merc_nor_thrall_skirmishers"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_merc_nor_wolf_coats"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_merc_rom_ballistarii"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_merc_rom_comitatensis_spears"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_merc_rom_contarii"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["att_merc_rom_equites_dalmatae"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["att_merc_rom_funditores"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_merc_rom_legio"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_merc_rom_onager"] = {
		culture = "other",
		class = "rich",
		count = 40
	},

	["att_merc_rom_palatina"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_merc_rom_sagittarii"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_nom_avar_horde"] = {
		culture = "other",
		class = "med",
		count = 80
	},

	["att_nom_bosphoran_infantry"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_nom_bosphoran_warriors"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_nom_chosen_uar_warriors"] = {
		culture = "other",
		class = "med",
		count = 160
	},

	["att_nom_civilian_farmers"] = {
		culture = "other",
		class = "poor",
		count = 40
	},

	["att_nom_civilian_townsfolk"] = {
		culture = "other",
		class = "poor",
		count = 40
	},

	["att_nom_elite_hunnic_lancers"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["att_nom_first_wave_lancers"] = {
		culture = "other",
		class = "med",
		count = 80
	},

	["att_nom_hunnic_ambushers"] = {
		culture = "other",
		class = "med",
		count = 80
	},

	["att_nom_hunnic_devil_archers"] = {
		culture = "other",
		class = "med",
		count = 80
	},

	["att_nom_hunnic_devil_cavalry"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["att_nom_hunnic_dismounted_warband"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_nom_hunnic_horse_archers"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["att_nom_hunnic_horsemen"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["att_nom_hunnic_lancers"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["att_nom_hunnic_mounted_bows"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["att_nom_hunnic_mounted_warband"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["att_nom_hunnic_spears"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_nom_hunnic_warlord"] = {
		culture = "other",
		class = "rich",
		count = 80
	},

	["att_nom_noble_acatziri_raiders"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["att_nom_noble_horse_archers"] = {
		culture = "other",
		class = "rich",
		count = 80
	},

	["att_nom_noble_steppe_cataphracts"] = {
		culture = "other",
		class = "rich",
		count = 80
	},

	["att_nom_nokkors"] = {
		culture = "other",
		class = "med",
		count = 80
	},

	["att_nom_onager"] = {
		culture = "other",
		class = "poor",
		count = 40
	},

	["att_nom_onager_bastion"] = {
		culture = "other",
		class = "poor",
		count = 4
	},

	["att_nom_onager_large"] = {
		culture = "other",
		class = "med",
		count = 40
	},

	["att_nom_scirii_warriors"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_nom_shamans_of_the_eternal_sky"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["att_nom_steppe_bows"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_nom_steppe_cataphracts"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["att_nom_steppe_chieftain"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["att_nom_steppe_horse_archers"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["att_nom_steppe_lancers"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["att_nom_steppe_levy"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_nom_steppe_mounted_bows"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["att_nom_steppe_mounted_brigands"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["att_nom_steppe_mounted_tribespeople"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["att_nom_steppe_raiders"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["att_nom_steppe_shield_archers"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_nom_steppe_spearmen"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_nom_steppe_tribespeople"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_nom_steppe_warlord"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["att_nom_steppe_warriors"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_nom_uar_archers"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_nom_uar_warriors"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_nom_unnigarde"] = {
		culture = "other",
		class = "med",
		count = 80
	},

	["att_nor_chieftain_with_gedriht"] = {
		culture = "other",
		class = "med",
		count = 160
	},

	["att_nor_chosen_warriors"] = {
		culture = "other",
		class = "med",
		count = 160
	},

	["att_nor_civilian_farmers"] = {
		culture = "other",
		class = "poor",
		count = 40
	},

	["att_nor_civilian_townsfolk"] = {
		culture = "other",
		class = "poor",
		count = 40
	},

	["att_nor_elite_duguth_spearmen"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_nor_elite_nordic_spearmen"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_nor_elite_nordic_warriors"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_nor_elite_saxon_javelinmen"] = {
		culture = "other",
		class = "med",
		count = 160
	},

	["att_nor_geoguth_javelinmen"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_nor_gesithas"] = {
		culture = "other",
		class = "med",
		count = 160
	},

	["att_nor_hearth_guard"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_nor_heroic_cavalry"] = {
		culture = "other",
		class = "med",
		count = 80
	},

	["att_nor_hirdmen"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_nor_huscarls"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_nor_nordic_axe_warband"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_nor_nordic_axe_warriors"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_nor_nordic_band"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_nor_nordic_bows"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_nor_nordic_brigade"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_nor_nordic_brigands"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_nor_nordic_horse_lords"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["att_nor_nordic_horse_raiders"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["att_nor_nordic_hurlers"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_nor_nordic_javelinmen"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_nor_nordic_levy"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_nor_nordic_mounted_raiders"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["att_nor_nordic_pikemen"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_nor_nordic_raid_leader"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_nor_nordic_raiders"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["att_nor_nordic_skirmishers"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_nor_nordic_spear_masters"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_nor_nordic_spearmen"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_nor_nordic_warlord"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_nor_nordic_warriors"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_nor_norse_berserkers"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_nor_onager"] = {
		culture = "other",
		class = "poor",
		count = 40
	},

	["att_nor_onager_bastion"] = {
		culture = "other",
		class = "poor",
		count = 4
	},

	["att_nor_onager_large"] = {
		culture = "other",
		class = "rich",
		count = 40
	},

	["att_nor_royal_huscarls"] = {
		culture = "other",
		class = "med",
		count = 160
	},

	["att_nor_saxon_lancers"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["att_nor_saxon_mounted_warband"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["att_nor_saxon_spears"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_nor_thrall_skirmishers"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_nor_thrall_spearmen"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_nor_thrall_spears"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_nor_viking_captain"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_nor_viking_raider_cavalry"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["att_nor_viking_raiders"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_nor_warhounds"] = {
		culture = "other",
		class = "poor",
		count = 40
	},

	["att_nor_wolf_coats"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_rom_armigeri_defensores"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_rom_armoured_sagittarii"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_rom_balistarii"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_rom_ballistarii"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_rom_catafractarii"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["att_rom_cheiroballistra"] = {
		culture = "other",
		class = "poor",
		count = 40
	},

	["att_rom_civilian_farmers"] = {
		culture = "other",
		class = "poor",
		count = 40
	},

	["att_rom_civilian_townsfolk"] = {
		culture = "other",
		class = "poor",
		count = 40
	},

	["att_rom_clibanarii"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["att_rom_cohors"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_rom_comes"] = {
		culture = "other",
		class = "med",
		count = 80
	},

	["att_rom_comitatensis_spears"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_rom_contarii"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["att_rom_cornuti_seniores"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_rom_eastern_armoured_legio"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_rom_eastern_auxilia_palatina"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_rom_elite_ballistarii"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_rom_elite_palatina"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_rom_equites_dalmatae"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["att_rom_equites_promoti"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["att_rom_equites_sagittarii"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["att_rom_excubitores_cavalry_guard"] = {
		culture = "other",
		class = "med",
		count = 80
	},

	["att_rom_exculcatores"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_rom_exploratores"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_rom_foederati_spears"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_rom_funditores"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_rom_herculiani_seniores"] = {
		culture = "other",
		class = "med",
		count = 160
	},

	["att_rom_hetaireia_guards"] = {
		culture = "other",
		class = "rich",
		count = 160
	},

	["att_rom_imperial_dromedarii"] = {
		culture = "other",
		class = "rich",
		count = 80
	},

	["att_rom_lanciarii_seniores"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_rom_legio"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_rom_legio_comitatenses"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_rom_levis_armaturae"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_rom_limitanei_borderguards"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_rom_magister_militum"] = {
		culture = "other",
		class = "rich",
		count = 80
	},

	["att_rom_matiarii"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_rom_menaulatoi"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_rom_noble_foederati_javelinmen"] = {
		culture = "other",
		class = "rich",
		count = 160
	},

	["att_rom_numeroi"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_rom_onager"] = {
		culture = "other",
		class = "poor",
		count = 40
	},

	["att_rom_onager_bastion"] = {
		culture = "other",
		class = "poor",
		count = 4
	},

	["att_rom_onager_large"] = {
		culture = "other",
		class = "rich",
		count = 40
	},

	["att_rom_palatina"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_rom_palatina_guards"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_rom_praeventores"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_rom_protectores_domestici"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_rom_ravenna_elite_ballistarii"] = {
		culture = "other",
		class = "med",
		count = 160
	},

	["att_rom_sagittarii"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_rom_scholae_gentiles"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["att_rom_scholae_palatinae"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["att_rom_scout_equites"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["att_rom_tagmata_cavalry"] = {
		culture = "other",
		class = "med",
		count = 80
	},

	["att_rom_western_auxilia_palatina"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_shp_alan_archers_lon_light"] = {
		culture = "other",
		class = "poor",
		count = 84
	},

	["att_shp_alan_artillery_dbl_lib_light"] = {
		culture = "other",
		class = "poor",
		count = 56
	},

	["att_shp_alan_artillery_lib_light"] = {
		culture = "other",
		class = "poor",
		count = 56
	},

	["att_shp_alan_boatment_dro_light_fs"] = {
		culture = "other",
		class = "poor",
		count = 36
	},

	["att_shp_alan_boatment_lib_light_ram"] = {
		culture = "other",
		class = "poor",
		count = 20
	},

	["att_shp_alan_boatment_lib_light_spike"] = {
		culture = "other",
		class = "poor",
		count = 36
	},

	["att_shp_alan_bow_lon_light"] = {
		culture = "other",
		class = "poor",
		count = 56
	},

	["att_shp_alan_bowmen_lon_light"] = {
		culture = "other",
		class = "poor",
		count = 96
	},

	["att_shp_alan_chosen_archers_dro_heavy"] = {
		culture = "other",
		class = "med",
		count = 156
	},

	["att_shp_alan_chosen_boatmen_lib_heavy_ft"] = {
		culture = "other",
		class = "rich",
		count = 140
	},

	["att_shp_alan_light_marauders_lon_light"] = {
		culture = "other",
		class = "poor",
		count = 56
	},

	["att_shp_alan_marauders_lon_light"] = {
		culture = "other",
		class = "poor",
		count = 72
	},

	["att_shp_cel_artillery_dbl_lib_light"] = {
		culture = "other",
		class = "poor",
		count = 56
	},

	["att_shp_cel_artillery_lib_light"] = {
		culture = "other",
		class = "poor",
		count = 56
	},

	["att_shp_cel_berserker_lon_heavy"] = {
		culture = "other",
		class = "poor",
		count = 128
	},

	["att_shp_cel_boatmen_dro_light_fs"] = {
		culture = "other",
		class = "poor",
		count = 32
	},

	["att_shp_cel_boatmen_lib_light_ram"] = {
		culture = "other",
		class = "poor",
		count = 20
	},

	["att_shp_cel_boatmen_lib_light_spike"] = {
		culture = "other",
		class = "poor",
		count = 36
	},

	["att_shp_cel_celtic_bow_lon_light"] = {
		culture = "other",
		class = "poor",
		count = 84
	},

	["att_shp_cel_chosen_celtic_lon_heavy"] = {
		culture = "other",
		class = "poor",
		count = 132
	},

	["att_shp_cel_ebdani_heavy_marauders"] = {
		culture = "other",
		class = "poor",
		count = 72
	},

	["att_shp_cel_elite_highland_bowman_marauders"] = {
		culture = "other",
		class = "poor",
		count = 84
	},

	["att_shp_cel_elite_norse_lon_heavy"] = {
		culture = "other",
		class = "poor",
		count = 132
	},

	["att_shp_cel_heavy_marauders_lon_light"] = {
		culture = "other",
		class = "poor",
		count = 72
	},

	["att_shp_cel_highland_archer_marauders"] = {
		culture = "other",
		class = "poor",
		count = 84
	},

	["att_shp_cel_light_marauders_lon_light"] = {
		culture = "other",
		class = "poor",
		count = 56
	},

	["att_shp_cel_marauders_lon_heavy"] = {
		culture = "other",
		class = "poor",
		count = 128
	},

	["att_shp_cel_pictish_berserker_marauders"] = {
		culture = "other",
		class = "poor",
		count = 132
	},

	["att_shp_cel_transport_heavy"] = {
		culture = "other",
		class = "poor",
		count = 124
	},

	["att_shp_cel_transport_light"] = {
		culture = "other",
		class = "poor",
		count = 64
	},

	["att_shp_des_assault_corsairs_dro_light_fs"] = {
		culture = "other",
		class = "poor",
		count = 32
	},

	["att_shp_des_assault_corsairs_lib_light_ram"] = {
		culture = "other",
		class = "poor",
		count = 20
	},

	["att_shp_des_assault_corsairs_lib_light_spike"] = {
		culture = "other",
		class = "poor",
		count = 36
	},

	["att_shp_des_corsairs_dro_light"] = {
		culture = "other",
		class = "poor",
		count = 72
	},

	["att_shp_des_heavy_corsairs_lib_light"] = {
		culture = "other",
		class = "poor",
		count = 72
	},

	["att_shp_des_naval_bowmen_dro_light"] = {
		culture = "other",
		class = "poor",
		count = 88
	},

	["att_shp_eas_artillery_dbl_dro_heavy"] = {
		culture = "other",
		class = "poor",
		count = 64
	},

	["att_shp_eas_artillery_dro_heavy"] = {
		culture = "other",
		class = "poor",
		count = 64
	},

	["att_shp_eas_artillery_lib_light"] = {
		culture = "other",
		class = "poor",
		count = 64
	},

	["att_shp_eas_boatmen_dro_heavy"] = {
		culture = "other",
		class = "poor",
		count = 140
	},

	["att_shp_eas_hurlers_dro_light"] = {
		culture = "other",
		class = "poor",
		count = 60
	},

	["att_shp_eas_light_boatmen_dro_light"] = {
		culture = "other",
		class = "poor",
		count = 68
	},

	["att_shp_er_archers_dro_light"] = {
		culture = "other",
		class = "poor",
		count = 92
	},

	["att_shp_er_archers_dro_light_sco"] = {
		culture = "other",
		class = "poor",
		count = 92
	},

	["att_shp_er_elite_marines_dro_heavy"] = {
		culture = "other",
		class = "poor",
		count = 140
	},

	["att_shp_er_elite_marines_dro_heavy_fs"] = {
		culture = "other",
		class = "med",
		count = 140
	},

	["att_shp_er_elite_marines_dro_heavy_ft"] = {
		culture = "other",
		class = "med",
		count = 140
	},

	["att_shp_er_light_marines_lib_heavy"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["att_shp_er_marines_dro_light_fs"] = {
		culture = "other",
		class = "poor",
		count = 40
	},

	["att_shp_fra_marines_lon_heavy"] = {
		culture = "other",
		class = "poor",
		count = 128
	},

	["att_shp_ger_archers_lon_light"] = {
		culture = "other",
		class = "poor",
		count = 84
	},

	["att_shp_ger_artillery_dbl_lib_light"] = {
		culture = "other",
		class = "poor",
		count = 56
	},

	["att_shp_ger_artillery_lib_light"] = {
		culture = "other",
		class = "poor",
		count = 56
	},

	["att_shp_ger_boatment_dro_light_fs"] = {
		culture = "other",
		class = "poor",
		count = 36
	},

	["att_shp_ger_boatment_lib_light_ram"] = {
		culture = "other",
		class = "poor",
		count = 20
	},

	["att_shp_ger_boatment_lib_light_spike"] = {
		culture = "other",
		class = "poor",
		count = 36
	},

	["att_shp_ger_bow_lon_light"] = {
		culture = "other",
		class = "poor",
		count = 56
	},

	["att_shp_ger_bowmen_lon_light"] = {
		culture = "other",
		class = "poor",
		count = 96
	},

	["att_shp_ger_chosen_archers_dro_heavy"] = {
		culture = "other",
		class = "med",
		count = 156
	},

	["att_shp_ger_chosen_boatmen_lib_heavy_ft"] = {
		culture = "other",
		class = "rich",
		count = 140
	},

	["att_shp_ger_light_marauders_lon_light"] = {
		culture = "other",
		class = "poor",
		count = 56
	},

	["att_shp_ger_marauders_lon_light"] = {
		culture = "other",
		class = "poor",
		count = 72
	},

	["att_shp_got_marines_lon_heavy"] = {
		culture = "other",
		class = "poor",
		count = 128
	},

	["att_shp_merc_cel_berserker_lon_heavy"] = {
		culture = "other",
		class = "poor",
		count = 128
	},

	["att_shp_merc_cel_celtic_bow_lon_light"] = {
		culture = "other",
		class = "poor",
		count = 84
	},

	["att_shp_merc_cel_heavy_marauders_lon_light"] = {
		culture = "other",
		class = "poor",
		count = 72
	},

	["att_shp_merc_cel_light_marauders_lon_light"] = {
		culture = "other",
		class = "poor",
		count = 56
	},

	["att_shp_merc_cel_marauders_lon_heavy"] = {
		culture = "other",
		class = "poor",
		count = 128
	},

	["att_shp_merc_des_corsairs_dro_light"] = {
		culture = "other",
		class = "poor",
		count = 72
	},

	["att_shp_merc_des_heavy_corsairs_lib_light"] = {
		culture = "other",
		class = "poor",
		count = 72
	},

	["att_shp_merc_eas_artillery_lib_light"] = {
		culture = "other",
		class = "poor",
		count = 64
	},

	["att_shp_merc_eas_hurlers_dro_light"] = {
		culture = "other",
		class = "poor",
		count = 60
	},

	["att_shp_merc_eas_light_boatmen_dro_light"] = {
		culture = "other",
		class = "poor",
		count = 68
	},

	["att_shp_merc_ger_artillery_lib_light"] = {
		culture = "other",
		class = "poor",
		count = 56
	},

	["att_shp_merc_ger_boatment_lib_light_ram"] = {
		culture = "other",
		class = "poor",
		count = 20
	},

	["att_shp_merc_ger_boatment_lib_light_spike"] = {
		culture = "other",
		class = "poor",
		count = 36
	},

	["att_shp_merc_ger_bow_lon_light"] = {
		culture = "other",
		class = "poor",
		count = 56
	},

	["att_shp_merc_ger_light_marauders_lon_light"] = {
		culture = "other",
		class = "poor",
		count = 56
	},

	["att_shp_merc_nor_artillery_lib_light"] = {
		culture = "other",
		class = "poor",
		count = 56
	},

	["att_shp_merc_nor_light_marauders_lon_light"] = {
		culture = "other",
		class = "poor",
		count = 56
	},

	["att_shp_merc_rom_artillery_lib_light"] = {
		culture = "other",
		class = "poor",
		count = 64
	},

	["att_shp_merc_rom_light_marines_lib_light"] = {
		culture = "other",
		class = "poor",
		count = 64
	},

	["att_shp_merc_rom_skirmisher_lib_light"] = {
		culture = "other",
		class = "poor",
		count = 64
	},

	["att_shp_merc_sax_bow_lon_light"] = {
		culture = "other",
		class = "poor",
		count = 92
	},

	["att_shp_merc_van_artillery_lib_light"] = {
		culture = "other",
		class = "poor",
		count = 64
	},

	["att_shp_merc_van_boatmen_lib_light_ram"] = {
		culture = "other",
		class = "poor",
		count = 20
	},

	["att_shp_merc_van_bow_lon_light_dagger"] = {
		culture = "other",
		class = "poor",
		count = 88
	},

	["att_shp_merc_van_light_marauders_lon_light"] = {
		culture = "other",
		class = "poor",
		count = 64
	},

	["att_shp_merc_vik_norse_bow_lon_light"] = {
		culture = "other",
		class = "poor",
		count = 84
	},

	["att_shp_nor_artillery_lib_light"] = {
		culture = "other",
		class = "poor",
		count = 56
	},

	["att_shp_nor_berserker_lon_heavy"] = {
		culture = "other",
		class = "poor",
		count = 128
	},

	["att_shp_nor_boatmen_dro_light_fs"] = {
		culture = "other",
		class = "poor",
		count = 32
	},

	["att_shp_nor_boatmen_lib_light_ram"] = {
		culture = "other",
		class = "poor",
		count = 20
	},

	["att_shp_nor_boatmen_lib_light_spike"] = {
		culture = "other",
		class = "poor",
		count = 36
	},

	["att_shp_nor_heavy_marauders_lon_light"] = {
		culture = "other",
		class = "poor",
		count = 72
	},

	["att_shp_nor_light_marauders_lon_light"] = {
		culture = "other",
		class = "poor",
		count = 56
	},

	["att_shp_nor_marauders_lon_heavy"] = {
		culture = "other",
		class = "poor",
		count = 128
	},

	["att_shp_nor_transport_heavy"] = {
		culture = "other",
		class = "poor",
		count = 124
	},

	["att_shp_nor_transport_light"] = {
		culture = "other",
		class = "poor",
		count = 64
	},

	["att_shp_ost_archers_dro_heavy"] = {
		culture = "other",
		class = "poor",
		count = 92
	},

	["att_shp_ost_elite_archers_lib_heavy"] = {
		culture = "other",
		class = "poor",
		count = 92
	},

	["att_shp_rom_artillery_dbl_lib_heavy"] = {
		culture = "other",
		class = "med",
		count = 64
	},

	["att_shp_rom_artillery_dbl_lib_light"] = {
		culture = "other",
		class = "poor",
		count = 64
	},

	["att_shp_rom_artillery_lib_heavy"] = {
		culture = "other",
		class = "poor",
		count = 64
	},

	["att_shp_rom_artillery_lib_light"] = {
		culture = "other",
		class = "poor",
		count = 64
	},

	["att_shp_rom_bowmen_lib_light"] = {
		culture = "other",
		class = "poor",
		count = 96
	},

	["att_shp_rom_heavy_marines_lib_heavy"] = {
		culture = "other",
		class = "poor",
		count = 148
	},

	["att_shp_rom_light_marines_lib_light"] = {
		culture = "other",
		class = "poor",
		count = 64
	},

	["att_shp_rom_skirmisher_lib_light"] = {
		culture = "other",
		class = "poor",
		count = 64
	},

	["att_shp_rom_transport_heavy"] = {
		culture = "other",
		class = "poor",
		count = 128
	},

	["att_shp_rom_transport_light"] = {
		culture = "other",
		class = "poor",
		count = 128
	},

	["att_shp_sas_archer_dro_light"] = {
		culture = "other",
		class = "poor",
		count = 92
	},

	["att_shp_sas_bowman_dro_light"] = {
		culture = "other",
		class = "poor",
		count = 92
	},

	["att_shp_sas_elite_archer_dro_heavy_dbl_sco"] = {
		culture = "other",
		class = "rich",
		count = 156
	},

	["att_shp_sas_elite_archer_dro_heavy_sco"] = {
		culture = "other",
		class = "med",
		count = 156
	},

	["att_shp_sas_elite_heavy_marines_dro_heavy_ft"] = {
		culture = "other",
		class = "rich",
		count = 140
	},

	["att_shp_sas_elite_light_marines_dro_heavy_fs"] = {
		culture = "other",
		class = "poor",
		count = 140
	},

	["att_shp_sas_light_marines_dro_light_fs"] = {
		culture = "other",
		class = "poor",
		count = 36
	},

	["att_shp_sas_naft_marines_dro_light"] = {
		culture = "other",
		class = "poor",
		count = 60
	},

	["att_shp_sas_slinger_dro_light"] = {
		culture = "other",
		class = "poor",
		count = 92
	},

	["att_shp_sax_bow_lon_light"] = {
		culture = "other",
		class = "poor",
		count = 84
	},

	["att_shp_sax_elite_lon_heavy"] = {
		culture = "other",
		class = "poor",
		count = 124
	},

	["att_shp_sax_royal_lon_heavy"] = {
		culture = "other",
		class = "poor",
		count = 124
	},

	["att_shp_slav_archers_lon_light"] = {
		culture = "other",
		class = "poor",
		count = 84
	},

	["att_shp_slav_artillery_dbl_lib_light"] = {
		culture = "other",
		class = "poor",
		count = 56
	},

	["att_shp_slav_artillery_lib_light"] = {
		culture = "other",
		class = "poor",
		count = 56
	},

	["att_shp_slav_boatment_dro_light_fs"] = {
		culture = "other",
		class = "poor",
		count = 36
	},

	["att_shp_slav_boatment_lib_light_ram"] = {
		culture = "other",
		class = "poor",
		count = 20
	},

	["att_shp_slav_boatment_lib_light_spike"] = {
		culture = "other",
		class = "poor",
		count = 36
	},

	["att_shp_slav_bow_lon_light"] = {
		culture = "other",
		class = "poor",
		count = 56
	},

	["att_shp_slav_bowmen_lon_light"] = {
		culture = "other",
		class = "poor",
		count = 96
	},

	["att_shp_slav_chosen_archers_dro_heavy"] = {
		culture = "other",
		class = "med",
		count = 156
	},

	["att_shp_slav_chosen_boatmen_lib_heavy_ft"] = {
		culture = "other",
		class = "rich",
		count = 140
	},

	["att_shp_slav_light_marauders_lon_light"] = {
		culture = "other",
		class = "poor",
		count = 56
	},

	["att_shp_slav_marauders_lon_light"] = {
		culture = "other",
		class = "poor",
		count = 72
	},

	["att_shp_van_artillery_dbl_lib_light"] = {
		culture = "other",
		class = "poor",
		count = 64
	},

	["att_shp_van_artillery_lib_heavy"] = {
		culture = "other",
		class = "poor",
		count = 64
	},

	["att_shp_van_artillery_lib_light"] = {
		culture = "other",
		class = "poor",
		count = 64
	},

	["att_shp_van_boatmen_dro_light_fs"] = {
		culture = "other",
		class = "poor",
		count = 48
	},

	["att_shp_van_boatmen_lib_light_ram"] = {
		culture = "other",
		class = "poor",
		count = 20
	},

	["att_shp_van_bow_lon_light_dagger"] = {
		culture = "other",
		class = "poor",
		count = 88
	},

	["att_shp_van_bowmen_lon_light_sword"] = {
		culture = "other",
		class = "poor",
		count = 88
	},

	["att_shp_van_chosen_marauders_lib_heavy_dbl_ft"] = {
		culture = "other",
		class = "rich",
		count = 148
	},

	["att_shp_van_chosen_marauders_lib_heavy_ft"] = {
		culture = "other",
		class = "rich",
		count = 148
	},

	["att_shp_van_elite_archers_dro_light_dbl_sco"] = {
		culture = "other",
		class = "poor",
		count = 96
	},

	["att_shp_van_elite_archers_dro_light_sco"] = {
		culture = "other",
		class = "poor",
		count = 96
	},

	["att_shp_van_elite_marauders_lon_heavy"] = {
		culture = "other",
		class = "poor",
		count = 128
	},

	["att_shp_van_heavy_marauders_lon_heavy"] = {
		culture = "other",
		class = "poor",
		count = 128
	},

	["att_shp_van_light_marauders_lon_light"] = {
		culture = "other",
		class = "poor",
		count = 64
	},

	["att_shp_van_marauders_lon_light"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["att_shp_vik_chosen_norse_lon_heavy"] = {
		culture = "other",
		class = "med",
		count = 132
	},

	["att_shp_vik_elite_norse_lon_heavy"] = {
		culture = "other",
		class = "poor",
		count = 132
	},

	["att_shp_vik_norse_bow_lon_light"] = {
		culture = "other",
		class = "poor",
		count = 84
	},

	["att_shp_vis_elite_crossbow_lib_heavy"] = {
		culture = "other",
		class = "poor",
		count = 148
	},

	["att_shp_vis_elite_marines_lon_heavy"] = {
		culture = "other",
		class = "poor",
		count = 132
	},

	["att_shp_wr_ballista_lib_heavy"] = {
		culture = "other",
		class = "poor",
		count = 156
	},

	["att_shp_wr_ballista_lib_heavy_dbl_sco"] = {
		culture = "other",
		class = "poor",
		count = 156
	},

	["att_shp_wr_ballista_lib_heavy_sco"] = {
		culture = "other",
		class = "poor",
		count = 156
	},

	["att_shp_wr_heavy_marines_lib_heavy_ft"] = {
		culture = "other",
		class = "rich",
		count = 148
	},

	["att_shp_wr_light_marines_lib_light_ram"] = {
		culture = "other",
		class = "poor",
		count = 20
	},

	["att_shp_wr_light_marines_lib_light_spike"] = {
		culture = "other",
		class = "poor",
		count = 36
	},

	["att_slav_ambushers"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_slav_archers"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_slav_axe_levy"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_slav_axe_warriors"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_slav_axemen"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_slav_champions"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_slav_chosen_shields"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_slav_followers_veles"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_slav_horse_butchers"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_slav_horse_cutters"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_slav_horse_raiders"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["att_slav_hunters"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_slav_large_shields"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_slav_levy_archers"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_slav_levy_skirmishers"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_slav_levy_spearmen"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_slav_noble_skirmishers"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_slav_noble_spearmen"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_slav_onager"] = {
		culture = "other",
		class = "poor",
		count = 40
	},

	["att_slav_onager_bastion"] = {
		culture = "other",
		class = "poor",
		count = 4
	},

	["att_slav_peruns_axes"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_slav_peruns_champions"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_slav_posion_archers"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_slav_posion_hunters"] = {
		culture = "other",
		class = "med",
		count = 160
	},

	["att_slav_raiders"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["att_slav_skirmishers"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_slav_spearmen"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_slav_svarogs_raiders"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["att_slav_veles_chosen"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_slav_warlords_guard"] = {
		culture = "other",
		class = "med",
		count = 160
	},

	["att_slav_warriors"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_wh_guardians_of_the_hindu_kush"] = {
		culture = "other",
		class = "rich",
		count = 160
	},

	["att_wh_hephthalite_chargers"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["att_wh_khingilas_khandas"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_wh_kindred_of_the_sun"] = {
		culture = "other",
		class = "rich",
		count = 80
	},

	["att_wh_spet_xyon_archers"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["att_wh_xionite_spears"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["att_wh_yanda_spearmasters"] = {
		culture = "other",
		class = "med",
		count = 160
	},

	["aus_late_serjeants"] = {
		culture = "other",
		class = "med",
		count = 160
	},

	["aus_pavise_crossbowmen"] = {
		culture = "other",
		class = "rich",
		count = 120
	},

	["aus_pavisiers"] = {
		culture = "other",
		class = "rich",
		count = 160
	},

	["austria_early_bodyguard"] = {
		culture = "other",
		class = "med",
		count = 50
	},

	["austria_high_bodyguard"] = {
		culture = "other",
		class = "med",
		count = 50
	},

	["austria_late_bodyguard"] = {
		culture = "other",
		class = "rich",
		count = 50
	},

	["austria_late_maa"] = {
		culture = "other",
		class = "rich",
		count = 160
	},

	["austrian_knights_early"] = {
		culture = "other",
		class = "med",
		count = 80
	},

	["austrian_knights_high"] = {
		culture = "other",
		class = "rich",
		count = 80
	},

	["austrian_knights_late"] = {
		culture = "other",
		class = "rich",
		count = 80
	},

	["axementier2"] = {
		culture = "other",
		class = "med",
		count = 160
	},

	["bal_ant_1_bodyguard"] = {
		culture = "other",
		class = "med",
		count = 50
	},

	["bal_ant_1_crossbowmen"] = {
		culture = "other",
		class = "med",
		count = 120
	},

	["bal_ant_1_heavy_inf"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["bal_ant_1_knights"] = {
		culture = "other",
		class = "med",
		count = 80
	},

	["bal_ant_1_knights_dismounted"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["bal_ant_1_sergeants_mounted"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["bal_ant_1_spearmen"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["bal_ant_1_swordsmen"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["bal_ant_1_syriac_archers"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["bal_ant_2_bodyguard"] = {
		culture = "other",
		class = "rich",
		count = 50
	},

	["bal_ant_2_crossbowmen"] = {
		culture = "other",
		class = "med",
		count = 120
	},

	["bal_ant_2_knights"] = {
		culture = "other",
		class = "med",
		count = 80
	},

	["bal_ant_2_knights_dismounted"] = {
		culture = "other",
		class = "med",
		count = 160
	},

	["bal_ant_2_sergeants_mounted"] = {
		culture = "other",
		class = "rich",
		count = 80
	},

	["bal_ant_2_spearmen"] = {
		culture = "other",
		class = "med",
		count = 160
	},

	["bal_ant_2_swordsmen"] = {
		culture = "other",
		class = "med",
		count = 160
	},

	["bal_cas_1_adventurers_french"] = {
		culture = "other",
		class = "med",
		count = 80
	},

	["bal_cas_1_adventurers_northern"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["bal_cas_1_almogavar_scouts"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["bal_cas_1_almogavars"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["bal_cas_1_bodyguard"] = {
		culture = "other",
		class = "poor",
		count = 50
	},

	["bal_cas_1_crossbows"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["bal_cas_1_jinetes"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["bal_cas_1_knight"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["bal_cas_1_knight_dismounted"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["bal_cas_1_moor_horsemen"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["bal_cas_1_moor_raiders"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["bal_cas_1_order_crossbows"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["bal_cas_1_order_knights"] = {
		culture = "other",
		class = "med",
		count = 80
	},

	["bal_cas_1_order_knights_dismounted"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["bal_cas_1_slingers"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["bal_cas_1_spearmen"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["bal_cas_2_adventurers_foot"] = {
		culture = "other",
		class = "med",
		count = 160
	},

	["bal_cas_2_adventurers_mounted"] = {
		culture = "other",
		class = "med",
		count = 80
	},

	["bal_cas_2_almogavar_scouts"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["bal_cas_2_almogavars"] = {
		culture = "other",
		class = "med",
		count = 160
	},

	["bal_cas_2_bodyguard"] = {
		culture = "other",
		class = "poor",
		count = 50
	},

	["bal_cas_2_crossbows"] = {
		culture = "other",
		class = "med",
		count = 120
	},

	["bal_cas_2_jinetes"] = {
		culture = "other",
		class = "rich",
		count = 80
	},

	["bal_cas_2_knight"] = {
		culture = "other",
		class = "med",
		count = 80
	},

	["bal_cas_2_knight_dismounted"] = {
		culture = "other",
		class = "med",
		count = 120
	},

	["bal_cas_2_moor_horsemen"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["bal_cas_2_moor_raiders"] = {
		culture = "other",
		class = "med",
		count = 120
	},

	["bal_cas_2_order_crossbows"] = {
		culture = "other",
		class = "med",
		count = 80
	},

	["bal_cas_2_order_knights"] = {
		culture = "other",
		class = "med",
		count = 80
	},

	["bal_cas_2_order_knights_dismounted"] = {
		culture = "other",
		class = "med",
		count = 120
	},

	["bal_cas_2_pikemen"] = {
		culture = "other",
		class = "med",
		count = 160
	},

	["bal_cas_2_spearmen"] = {
		culture = "other",
		class = "med",
		count = 160
	},

	["bal_cas_3_adventurers"] = {
		culture = "other",
		class = "med",
		count = 160
	},

	["bal_cas_3_adventurers_mounted"] = {
		culture = "other",
		class = "rich",
		count = 80
	},

	["bal_cas_3_bodyguard"] = {
		culture = "other",
		class = "med",
		count = 50
	},

	["bal_cas_3_crossbows_pavise"] = {
		culture = "other",
		class = "rich",
		count = 120
	},

	["bal_cas_3_halberdiers"] = {
		culture = "other",
		class = "rich",
		count = 160
	},

	["bal_cas_3_handgunners"] = {
		culture = "other",
		class = "rich",
		count = 120
	},

	["bal_cas_3_jinetes"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["bal_cas_3_knights"] = {
		culture = "other",
		class = "rich",
		count = 80
	},

	["bal_cas_3_knights_dismounted"] = {
		culture = "other",
		class = "rich",
		count = 160
	},

	["bal_cas_3_order_crossbows"] = {
		culture = "other",
		class = "med",
		count = 80
	},

	["bal_cas_3_order_knights"] = {
		culture = "other",
		class = "rich",
		count = 80
	},

	["bal_cas_3_order_knights_dismounted"] = {
		culture = "other",
		class = "rich",
		count = 160
	},

	["bal_cas_3_pikemen"] = {
		culture = "other",
		class = "rich",
		count = 160
	},

	["bal_cas_3_rodeleros"] = {
		culture = "other",
		class = "med",
		count = 160
	},

	["bal_cas_3_scouts"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["bal_cas_3_spearmen"] = {
		culture = "other",
		class = "rich",
		count = 160
	},

	["bal_jer_1_bodyguard_baron"] = {
		culture = "other",
		class = "med",
		count = 50
	},

	["bal_jer_1_crusader_crossbows_mounted"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["bal_jer_1_crusader_knights"] = {
		culture = "other",
		class = "med",
		count = 80
	},

	["bal_jer_1_crusader_knights_dismounted"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["bal_jer_1_crusader_marksmen"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["bal_jer_1_crusader_northmen"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["bal_jer_1_hospitaller_bodyguard"] = {
		culture = "other",
		class = "med",
		count = 50
	},

	["bal_jer_1_hospitaller_caravan"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["bal_jer_1_hospitaller_knights"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["bal_jer_1_hospitaller_knights_dismounted"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["bal_jer_1_ibelin_bodyguard"] = {
		culture = "other",
		class = "med",
		count = 50
	},

	["bal_jer_1_lombard_cavalry"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["bal_jer_1_maronite_spearmen"] = {
		culture = "other",
		class = "poor",
		count = 180
	},

	["bal_jer_1_ministeriales"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["bal_jer_1_order_cavalry"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["bal_jer_1_order_swordsmen"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["bal_jer_1_poulain_knights"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["bal_jer_1_poulain_knights_dismounted"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["bal_jer_1_spearmen_pisan"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["bal_jer_1_swordsmen_french"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["bal_jer_1_syriac_crossbowmen"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["bal_jer_1_templar_bodyguard"] = {
		culture = "other",
		class = "med",
		count = 50
	},

	["bal_jer_1_templar_knights"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["bal_jer_1_templar_knights_dismounted"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["bal_jer_1_turcopoles_dismounted"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["bal_jer_1_turcopoles_mounted"] = {
		culture = "other",
		class = "med",
		count = 80
	},

	["bal_jer_1_voulgier"] = {
		culture = "other",
		class = "poor",
		count = 180
	},

	["bal_jer_2_armenian_cavalry"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["bal_jer_2_bodyguard_baron"] = {
		culture = "other",
		class = "rich",
		count = 50
	},

	["bal_jer_2_crusader_crossbows_mounted"] = {
		culture = "other",
		class = "med",
		count = 120
	},

	["bal_jer_2_crusader_halberdiers"] = {
		culture = "other",
		class = "med",
		count = 160
	},

	["bal_jer_2_crusader_knights"] = {
		culture = "other",
		class = "rich",
		count = 80
	},

	["bal_jer_2_crusader_knights_dismounted"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["bal_jer_2_crusader_marksmen"] = {
		culture = "other",
		class = "med",
		count = 120
	},

	["bal_jer_2_hospitaller_bodyguard"] = {
		culture = "other",
		class = "med",
		count = 50
	},

	["bal_jer_2_hospitaller_caravan"] = {
		culture = "other",
		class = "rich",
		count = 120
	},

	["bal_jer_2_hospitaller_knights"] = {
		culture = "other",
		class = "rich",
		count = 80
	},

	["bal_jer_2_hospitaller_knights_dismounted"] = {
		culture = "other",
		class = "med",
		count = 160
	},

	["bal_jer_2_king_bodyguard"] = {
		culture = "other",
		class = "rich",
		count = 50
	},

	["bal_jer_2_ministeriales"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["bal_jer_2_order_cavalry"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["bal_jer_2_order_swordsmen"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["bal_jer_2_poulain_knights"] = {
		culture = "other",
		class = "rich",
		count = 80
	},

	["bal_jer_2_poulain_knights_dismounted"] = {
		culture = "other",
		class = "med",
		count = 160
	},

	["bal_jer_2_spearmen"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["bal_jer_2_spearmen_venetian"] = {
		culture = "other",
		class = "med",
		count = 160
	},

	["bal_jer_2_swordsmen_french"] = {
		culture = "other",
		class = "med",
		count = 160
	},

	["bal_jer_2_syriac_crossbowmen"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["bal_jer_2_templar_bodyguard"] = {
		culture = "other",
		class = "med",
		count = 50
	},

	["bal_jer_2_templar_knights"] = {
		culture = "other",
		class = "rich",
		count = 80
	},

	["bal_jer_2_templar_knights_dismounted"] = {
		culture = "other",
		class = "med",
		count = 160
	},

	["bal_jer_2_turcopoles_dismounted"] = {
		culture = "other",
		class = "med",
		count = 120
	},

	["bal_jer_2_turcopoles_mounted"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["bal_jer_2_voulgier"] = {
		culture = "other",
		class = "med",
		count = 160
	},

	["bal_jer_3_bodyguard_baron"] = {
		culture = "other",
		class = "rich",
		count = 50
	},

	["bal_jer_3_crossbowmen"] = {
		culture = "other",
		class = "rich",
		count = 120
	},

	["bal_jer_3_crusader_crossbows"] = {
		culture = "other",
		class = "rich",
		count = 120
	},

	["bal_jer_3_crusader_knights"] = {
		culture = "other",
		class = "rich",
		count = 80
	},

	["bal_jer_3_crusader_knights_dismounted"] = {
		culture = "other",
		class = "rich",
		count = 160
	},

	["bal_jer_3_crusader_marksmen"] = {
		culture = "other",
		class = "rich",
		count = 120
	},

	["bal_jer_3_crusader_pikemen"] = {
		culture = "other",
		class = "rich",
		count = 160
	},

	["bal_jer_3_greek_cavalry"] = {
		culture = "other",
		class = "rich",
		count = 80
	},

	["bal_jer_3_halberdiers"] = {
		culture = "other",
		class = "rich",
		count = 160
	},

	["bal_jer_3_hospitaller_bodyguard"] = {
		culture = "other",
		class = "rich",
		count = 90
	},

	["bal_jer_3_hospitaller_caravan"] = {
		culture = "other",
		class = "rich",
		count = 120
	},

	["bal_jer_3_hospitaller_caravan_crossbow"] = {
		culture = "other",
		class = "rich",
		count = 120
	},

	["bal_jer_3_hospitaller_knights"] = {
		culture = "other",
		class = "rich",
		count = 80
	},

	["bal_jer_3_hospitaller_knights_dismounted"] = {
		culture = "other",
		class = "rich",
		count = 160
	},

	["bal_jer_3_king_bodyguard"] = {
		culture = "other",
		class = "rich",
		count = 50
	},

	["bal_jer_3_ministeriales"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["bal_jer_3_order_cavalry"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["bal_jer_3_order_swordsmen"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["bal_jer_3_poulain_knights"] = {
		culture = "other",
		class = "rich",
		count = 80
	},

	["bal_jer_3_poulain_knights_dismounted"] = {
		culture = "other",
		class = "med",
		count = 160
	},

	["bal_jer_3_spearmen"] = {
		culture = "other",
		class = "rich",
		count = 160
	},

	["bal_jer_3_spearmen_genoese"] = {
		culture = "other",
		class = "rich",
		count = 160
	},

	["bal_jer_3_swordsmen_french"] = {
		culture = "other",
		class = "med",
		count = 160
	},

	["bal_jer_3_templar_bodyguard"] = {
		culture = "other",
		class = "rich",
		count = 50
	},

	["bal_jer_3_templar_knights"] = {
		culture = "other",
		class = "rich",
		count = 80
	},

	["bal_jer_3_templar_knights_dismounted"] = {
		culture = "other",
		class = "rich",
		count = 160
	},

	["bal_jer_3_turcopoles"] = {
		culture = "other",
		class = "med",
		count = 80
	},

	["bal_jer_3_turcopoles_dismounted"] = {
		culture = "other",
		class = "med",
		count = 120
	},

	["bal_jer_3_turcopoles_gunners"] = {
		culture = "other",
		class = "rich",
		count = 120
	},

	["bal_jer_crusader_crossbowmen"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["bal_jer_crusader_dismounted_sergeants"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["bal_jer_crusader_foot_knights"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["bal_jer_crusader_knights"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["bal_jer_crusader_marksmen"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["bal_jer_crusader_northmen"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["bal_jer_crusader_sergeants"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["bal_jer_foot_turcopoles"] = {
		culture = "other",
		class = "med",
		count = 120
	},

	["bal_jer_he_communal_militia"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["bal_jer_he_crusader_crossbowmen"] = {
		culture = "other",
		class = "med",
		count = 120
	},

	["bal_jer_he_crusader_knight_swordsmen"] = {
		culture = "other",
		class = "med",
		count = 160
	},

	["bal_jer_he_crusader_knights"] = {
		culture = "other",
		class = "rich",
		count = 80
	},

	["bal_jer_he_crusader_marksmen"] = {
		culture = "other",
		class = "med",
		count = 120
	},

	["bal_jer_he_crusader_spearmen"] = {
		culture = "other",
		class = "med",
		count = 160
	},

	["bal_jer_he_hospitaller_caravan"] = {
		culture = "other",
		class = "rich",
		count = 120
	},

	["bal_jer_he_hospitaller_knights"] = {
		culture = "other",
		class = "rich",
		count = 80
	},

	["bal_jer_he_kingsguard_of_jerusalem"] = {
		culture = "other",
		class = "rich",
		count = 50
	},

	["bal_jer_he_lombard_cavalry"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["bal_jer_he_maronite_spearmen"] = {
		culture = "other",
		class = "med",
		count = 160
	},

	["bal_jer_he_ministeriale_spears"] = {
		culture = "other",
		class = "med",
		count = 160
	},

	["bal_jer_he_ministeriales"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["bal_jer_he_mounted_crossbowmen"] = {
		culture = "other",
		class = "med",
		count = 80
	},

	["bal_jer_he_order_swordsmen"] = {
		culture = "other",
		class = "med",
		count = 160
	},

	["bal_jer_he_poulain_crossbowmen"] = {
		culture = "other",
		class = "med",
		count = 120
	},

	["bal_jer_he_poulain_dismounted_knights"] = {
		culture = "other",
		class = "med",
		count = 160
	},

	["bal_jer_he_poulain_knights"] = {
		culture = "other",
		class = "rich",
		count = 80
	},

	["bal_jer_he_templar_dismounted_knights"] = {
		culture = "other",
		class = "med",
		count = 160
	},

	["bal_jer_he_templar_knights"] = {
		culture = "other",
		class = "rich",
		count = 80
	},

	["bal_jer_hospitaller_donati"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["bal_jer_hospitaller_he_foot_knights"] = {
		culture = "other",
		class = "med",
		count = 160
	},

	["bal_jer_hospitaller_knight_dismounts"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["bal_jer_hospitaller_knights"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["bal_jer_hospitaller_sergeant_crossbowmen"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["bal_jer_hospitaller_sergeant_dismounts"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["bal_jer_hospitaller_sergeants"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["bal_jer_jerusalem_kingsguard"] = {
		culture = "other",
		class = "med",
		count = 50
	},

	["bal_jer_la_communal_militia"] = {
		culture = "other",
		class = "poor",
		count = 180
	},

	["bal_jer_la_crusader_knight"] = {
		culture = "other",
		class = "rich",
		count = 80
	},

	["bal_jer_la_crusader_manatarms"] = {
		culture = "other",
		class = "rich",
		count = 160
	},

	["bal_jer_la_crusader_marksmen"] = {
		culture = "other",
		class = "rich",
		count = 120
	},

	["bal_jer_la_french_regiment"] = {
		culture = "other",
		class = "rich",
		count = 80
	},

	["bal_jer_la_genoese_marines"] = {
		culture = "other",
		class = "rich",
		count = 160
	},

	["bal_jer_la_greek_refugees"] = {
		culture = "other",
		class = "rich",
		count = 80
	},

	["bal_jer_la_gunners"] = {
		culture = "other",
		class = "rich",
		count = 120
	},

	["bal_jer_la_hospitaller_caravan"] = {
		culture = "other",
		class = "rich",
		count = 120
	},

	["bal_jer_la_hospitaller_knight"] = {
		culture = "other",
		class = "rich",
		count = 80
	},

	["bal_jer_la_hospitaller_manatarms"] = {
		culture = "other",
		class = "rich",
		count = 160
	},

	["bal_jer_la_kingsguard"] = {
		culture = "other",
		class = "rich",
		count = 50
	},

	["bal_jer_la_poulain_knight"] = {
		culture = "other",
		class = "rich",
		count = 80
	},

	["bal_jer_la_poulain_menatarms"] = {
		culture = "other",
		class = "rich",
		count = 160
	},

	["bal_jer_la_templar_knight"] = {
		culture = "other",
		class = "rich",
		count = 80
	},

	["bal_jer_la_templar_manatarms"] = {
		culture = "other",
		class = "rich",
		count = 160
	},

	["bal_jer_maronite_skirmishers"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["bal_jer_maronite_spearmen"] = {
		culture = "other",
		class = "poor",
		count = 180
	},

	["bal_jer_ministeriale_spearmen"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["bal_jer_ministeriales"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["bal_jer_order_cavalry"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["bal_jer_order_swordsmen"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["bal_jer_pisan_marine"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["bal_jer_poulain_crossbowmen"] = {
		culture = "other",
		class = "med",
		count = 120
	},

	["bal_jer_poulain_dismounts"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["bal_jer_poulain_knights"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["bal_jer_templar_confraters"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["bal_jer_templar_crossbow_sergeants"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["bal_jer_templar_foot_knight"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["bal_jer_templar_foot_sergeant"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["bal_jer_templar_knight"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["bal_jer_templar_mounted_crossbows"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["bal_jer_templar_sergeant"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["bal_jer_turcopoles"] = {
		culture = "other",
		class = "med",
		count = 80
	},

	["bal_tou_1_cathar_mounted"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["bal_tou_1_cathar_spears"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["bal_tou_1_crossbows_dismounted"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["bal_tou_1_crossbows_mounted"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["bal_tou_1_knights_bodyguard"] = {
		culture = "other",
		class = "poor",
		count = 50
	},

	["bal_tou_1_knights_bodyguard_trencavel"] = {
		culture = "other",
		class = "poor",
		count = 50
	},

	["bal_tou_1_knights_dismounted"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["bal_tou_1_knights_mounted"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["bal_tou_1_perfect_dismounted"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["bal_tou_1_perfect_mounted"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["bal_tou_1_routier_bow"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["bal_tou_1_routiers_heavy"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["bal_tou_1_routiers_heavy_foot"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["bal_tou_1_routiers_light"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["bal_tou_1_routiers_light_foot"] = {
		culture = "other",
		class = "poor",
		count = 200
	},

	["bal_tou_1_sergeants_foot"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["bal_tou_1_sergeants_mounted"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["bal_tou_1_sergeants_spears"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["bal_tou_1_voulge_militia"] = {
		culture = "other",
		class = "poor",
		count = 180
	},

	["bal_tou_2_cathar_mounted"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["bal_tou_2_cathar_swords"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["bal_tou_2_crossbows_dismounted"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["bal_tou_2_crossbows_mounted"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["bal_tou_2_freecompany_archers"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["bal_tou_2_freecompany_footmen"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["bal_tou_2_freecompany_mounted"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["bal_tou_2_knight_dismounted"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["bal_tou_2_knight_mounted"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["bal_tou_2_knights_bodyguard"] = {
		culture = "other",
		class = "poor",
		count = 50
	},

	["bal_tou_2_perfect_dismounted"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["bal_tou_2_perfect_mounted"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["bal_tou_2_sergeants_mounted"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["bal_tou_2_sergeants_spears"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["bal_tou_2_sergeants_swords"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["bal_tou_2_voulge_sergeants"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["bal_tou_3_cathar_mounted"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["bal_tou_3_cathar_spearmen"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["bal_tou_3_cathar_voulgier"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["bal_tou_3_crossbows"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["bal_tou_3_crossbows_mounted"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["bal_tou_3_gunners"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["bal_tou_3_knights_bodyguard"] = {
		culture = "other",
		class = "poor",
		count = 50
	},

	["bal_tou_3_knights_dismounted"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["bal_tou_3_knights_mounted"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["bal_tou_3_perfect_dismounted"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["bal_tou_3_perfect_mounted"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["bal_tou_3_sergeant_mounted"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["bal_tou_3_sergeant_spearmen"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["bal_tou_3_voulgier"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["bar_castellans"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["bar_crossbowmen"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["bar_maa"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["bar_mounted_maa"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["bar_mounted_serjeants"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["bar_retinue"] = {
		culture = "other",
		class = "med",
		count = 50
	},

	["bar_spearmen"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["bar_voulgier"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["bardax_retinue"] = {
		culture = "other",
		class = "med",
		count = 50
	},

	["beauv_castellans"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["beauv_crossbowmen"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["beauv_maa"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["beauv_mounted_maa"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["beauv_mounted_serjeants"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["beauv_retinue"] = {
		culture = "other",
		class = "med",
		count = 50
	},

	["beauv_spearmen"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["beauv_voulgier"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["bel_ost_ballistarii"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["bel_ost_cheiroballista"] = {
		culture = "other",
		class = "poor",
		count = 40
	},

	["bel_ost_comes"] = {
		culture = "other",
		class = "med",
		count = 80
	},

	["bel_ost_domestici"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["bel_ost_elite_ostrogothic_pikemen"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["bel_ost_elite_sagittarii"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["bel_ost_gothic_horsemen"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["bel_ost_gothic_lancers"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["bel_ost_gothic_pikemen"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["bel_ost_iuvenes"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["bel_ost_javelinmen"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["bel_ost_large_onager"] = {
		culture = "other",
		class = "rich",
		count = 40
	},

	["bel_ost_milites"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["bel_ost_milites_comitatenses"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["bel_ost_noble_gothic_landers"] = {
		culture = "other",
		class = "med",
		count = 80
	},

	["bel_ost_noble_horsemen"] = {
		culture = "other",
		class = "med",
		count = 80
	},

	["bel_ost_noble_javelinmen"] = {
		culture = "other",
		class = "rich",
		count = 160
	},

	["bel_ost_onager"] = {
		culture = "other",
		class = "poor",
		count = 40
	},

	["bel_ost_onager_bastion"] = {
		culture = "other",
		class = "poor",
		count = 4
	},

	["bel_ost_sagittarii"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["bel_ost_scholae"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["bel_ost_spear_milites"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["bel_ost_spear_milites_comitatenses"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["bel_ost_spear_veterans"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["bel_ost_veterans"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["bel_rom_antesignani"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["bel_rom_armoured_sagittarii"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["bel_rom_ballistarii"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["bel_rom_bucellarii"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["bel_rom_bucellarii_guard_axemen"] = {
		culture = "other",
		class = "med",
		count = 160
	},

	["bel_rom_bucellarii_guard_cavalry"] = {
		culture = "other",
		class = "med",
		count = 80
	},

	["bel_rom_cataphracti"] = {
		culture = "other",
		class = "med",
		count = 80
	},

	["bel_rom_cheiroballista"] = {
		culture = "other",
		class = "poor",
		count = 40
	},

	["bel_rom_clibanarii"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["bel_rom_elite_ballistarii"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["bel_rom_equites_promoti"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["bel_rom_exploratores"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["bel_rom_foederati"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["bel_rom_foederati_cav"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["bel_rom_foederati_horse_archers"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["bel_rom_funditores"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["bel_rom_hippo_toxotai"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["bel_rom_imperial_dromedarii"] = {
		culture = "other",
		class = "rich",
		count = 80
	},

	["bel_rom_large_onager"] = {
		culture = "other",
		class = "rich",
		count = 40
	},

	["bel_rom_levis_armaturae"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["bel_rom_limitanei"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["bel_rom_onager"] = {
		culture = "other",
		class = "poor",
		count = 40
	},

	["bel_rom_onager_bastion"] = {
		culture = "other",
		class = "poor",
		count = 4
	},

	["bel_rom_praeventores"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["bel_rom_sagittarii"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["bel_rom_scout_equites"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["bel_rom_skutatoi"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["bel_rom_skutatoi_axemen"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["bel_rom_skutatoi_spears"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["bel_rom_tagmata_cavalry"] = {
		culture = "other",
		class = "med",
		count = 80
	},

	["bel_van_moorish_levy"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["bel_van_noble_vandal_horsemen"] = {
		culture = "other",
		class = "rich",
		count = 80
	},

	["bel_van_noble_vandal_swordsmen"] = {
		culture = "other",
		class = "rich",
		count = 160
	},

	["bel_van_vandal_horsemen"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["bel_van_vandali_comitatus"] = {
		culture = "other",
		class = "rich",
		count = 80
	},

	["bernhalberds"] = {
		culture = "other",
		class = "med",
		count = 160
	},

	["bernmacemen"] = {
		culture = "other",
		class = "med",
		count = 160
	},

	["bigor_retinue"] = {
		culture = "other",
		class = "med",
		count = 50
	},

	["bill_militia"] = {
		culture = "other",
		class = "poor",
		count = 180
	},

	["boulogne_castellans"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["boulogne_crossbowmen"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["boulogne_maa"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["boulogne_mounted_maa"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["boulogne_mounted_serjeants"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["boulogne_retinue"] = {
		culture = "other",
		class = "med",
		count = 50
	},

	["boulogne_spearmen"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["boulogne_voulgier"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["briene_castellans"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["briene_crossbowmen"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["briene_maa"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["briene_mounted_maa"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["briene_mounted_serjeants"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["briene_retinue"] = {
		culture = "other",
		class = "med",
		count = 50
	},

	["briene_spearmen"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["briene_voulgier"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["brittany_castellans"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["brittany_crossbowmen"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["brittany_maa"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["brittany_mounted_maa"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["brittany_mounted_serjeants"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["brittany_retinue"] = {
		culture = "other",
		class = "med",
		count = 50
	},

	["brittany_spearmen"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["brittany_voulgier"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["bur_coustiliers"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["bur_crossbow_militia"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["bur_grendarmes"] = {
		culture = "other",
		class = "rich",
		count = 80
	},

	["bur_halberdiers"] = {
		culture = "other",
		class = "rich",
		count = 160
	},

	["bur_handgunners"] = {
		culture = "other",
		class = "rich",
		count = 120
	},

	["bur_high_maa"] = {
		culture = "other",
		class = "rich",
		count = 160
	},

	["bur_high_mounted_maa"] = {
		culture = "other",
		class = "rich",
		count = 80
	},

	["bur_late_retinue"] = {
		culture = "other",
		class = "rich",
		count = 50
	},

	["bur_pavise_crossbowmen"] = {
		culture = "other",
		class = "rich",
		count = 120
	},

	["bur_pavisiers"] = {
		culture = "other",
		class = "rich",
		count = 160
	},

	["bur_pikemen"] = {
		culture = "other",
		class = "rich",
		count = 160
	},

	["bur_scouts"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["bur_spear_militia"] = {
		culture = "other",
		class = "poor",
		count = 180
	},

	["bur_voulge_militia"] = {
		culture = "other",
		class = "poor",
		count = 180
	},

	["burgundy_castellans"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["burgundy_crossbowmen"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["burgundy_maa"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["burgundy_mid_castellans"] = {
		culture = "other",
		class = "med",
		count = 160
	},

	["burgundy_mid_crossbowmen"] = {
		culture = "other",
		class = "med",
		count = 120
	},

	["burgundy_mid_mounted_serjeants"] = {
		culture = "other",
		class = "rich",
		count = 80
	},

	["burgundy_mid_spearmen"] = {
		culture = "other",
		class = "med",
		count = 160
	},

	["burgundy_mid_voulgier"] = {
		culture = "other",
		class = "med",
		count = 160
	},

	["burgundy_mounted_crossbowmen"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["burgundy_mounted_crossbowmen_high"] = {
		culture = "other",
		class = "med",
		count = 80
	},

	["burgundy_mounted_crossbowmen_late"] = {
		culture = "other",
		class = "med",
		count = 80
	},

	["burgundy_mounted_maa"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["burgundy_mounted_serjeants"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["burgundy_retinue"] = {
		culture = "other",
		class = "med",
		count = 50
	},

	["burgundy_spearmen"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["burgundy_voulgier"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["castellou_retinue"] = {
		culture = "other",
		class = "med",
		count = 50
	},

	["catalan_crossbow_militia"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["catalan_knights"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["catalan_spear_militia"] = {
		culture = "other",
		class = "poor",
		count = 180
	},

	["catalan_swordsmen"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["cha_avar_dragonship_guard_marines"] = {
		culture = "other",
		class = "poor",
		count = 128
	},

	["cha_avar_foot_archers"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["cha_avar_guard"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["cha_avar_horse_archers"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["cha_avar_horse_masters"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["cha_avar_lancers"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["cha_avar_levy_archers"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["cha_avar_levy_spear"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["cha_avar_longship_boatmen"] = {
		culture = "other",
		class = "poor",
		count = 128
	},

	["cha_avar_longship_heavy_boatmen"] = {
		culture = "other",
		class = "poor",
		count = 128
	},

	["cha_avar_longship_light_boatmen"] = {
		culture = "other",
		class = "poor",
		count = 88
	},

	["cha_avar_noble_archers"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["cha_avar_noble_lancers"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["cha_avar_noble_sword"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["cha_avar_onager"] = {
		culture = "other",
		class = "poor",
		count = 40
	},

	["cha_avar_onager_bastion"] = {
		culture = "other",
		class = "poor",
		count = 4
	},

	["cha_avar_spear"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["cha_avar_tribesmen"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["cha_bre_armorican_cav"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["cha_bre_armorican_knights"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["cha_byz_archers"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["cha_byz_armoured_archers"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["cha_byz_artillery_dromon"] = {
		culture = "other",
		class = "poor",
		count = 64
	},

	["cha_byz_castle_dromon_guard_marines"] = {
		culture = "other",
		class = "poor",
		count = 128
	},

	["cha_byz_dromon_archer_boatmen"] = {
		culture = "other",
		class = "poor",
		count = 88
	},

	["cha_byz_dromon_levy_marines"] = {
		culture = "other",
		class = "poor",
		count = 128
	},

	["cha_byz_dromon_skutatoi"] = {
		culture = "other",
		class = "poor",
		count = 128
	},

	["cha_byz_duxs_guard"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["cha_byz_iuvenes"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["cha_byz_large_onager"] = {
		culture = "other",
		class = "poor",
		count = 40
	},

	["cha_byz_levy_spear"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["cha_byz_light_cav"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["cha_byz_onager"] = {
		culture = "other",
		class = "poor",
		count = 40
	},

	["cha_byz_onager_bastion"] = {
		culture = "other",
		class = "poor",
		count = 4
	},

	["cha_byz_scout_cavalry"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["cha_byz_skirm"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["cha_byz_skutatoi"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["cha_byz_skutatoi_axemen"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["cha_byz_skutatoi_spear"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["cha_byz_tagmata_cavalry"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["cha_cel_armoured_spear"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["cha_cel_axemen"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["cha_cel_currach_teulu_marines"] = {
		culture = "other",
		class = "poor",
		count = 128
	},

	["cha_cel_dragonship_teulu_marines"] = {
		culture = "other",
		class = "poor",
		count = 128
	},

	["cha_cel_fianna"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["cha_cel_gazelhounds"] = {
		culture = "other",
		class = "poor",
		count = 40
	},

	["cha_cel_horsemen"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["cha_cel_javelinmen"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["cha_cel_kerns"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["cha_cel_kings_fianna"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["cha_cel_kings_warband"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["cha_cel_levy_archers"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["cha_cel_levy_spear"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["cha_cel_long_fhada__sword_boatmen"] = {
		culture = "other",
		class = "poor",
		count = 128
	},

	["cha_cel_long_fhada_archer_boatmen"] = {
		culture = "other",
		class = "poor",
		count = 88
	},

	["cha_cel_long_fhada_javelin_boatmen"] = {
		culture = "other",
		class = "poor",
		count = 88
	},

	["cha_cel_long_fhada_spear_boatmen"] = {
		culture = "other",
		class = "poor",
		count = 128
	},

	["cha_cel_longship_archer_boatmen"] = {
		culture = "other",
		class = "poor",
		count = 88
	},

	["cha_cel_longship_javelin_boatmen"] = {
		culture = "other",
		class = "poor",
		count = 88
	},

	["cha_cel_longship_spear_boatmen"] = {
		culture = "other",
		class = "poor",
		count = 128
	},

	["cha_cel_longship_sword_boatmen"] = {
		culture = "other",
		class = "poor",
		count = 128
	},

	["cha_cel_onager"] = {
		culture = "other",
		class = "poor",
		count = 40
	},

	["cha_cel_onager_bastion"] = {
		culture = "other",
		class = "poor",
		count = 4
	},

	["cha_cel_pictish_archers"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["cha_cel_pictish_crossbowmen"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["cha_cel_pictish_nobles"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["cha_cel_royal_horsemen"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["cha_cel_spear"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["cha_cel_sword"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["cha_cel_sword_band"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["cha_cel_sword_warriors"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["cha_cel_teulu"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["cha_cel_welsh_archers"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["cha_cel_welsh_longbowmen"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["cha_eng_dragonship_royal_marines"] = {
		culture = "other",
		class = "poor",
		count = 128
	},

	["cha_eng_fyrd_archers"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["cha_eng_fyrd_axemen"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["cha_eng_fyrd_javelinmen"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["cha_eng_fyrd_slingers"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["cha_eng_fyrd_spear"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["cha_eng_horsemen"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["cha_eng_longship_fyrd_archer_boatmen"] = {
		culture = "other",
		class = "poor",
		count = 88
	},

	["cha_eng_longship_fyrd_boatmen"] = {
		culture = "other",
		class = "poor",
		count = 128
	},

	["cha_eng_longship_thegn_boatmen"] = {
		culture = "other",
		class = "poor",
		count = 128
	},

	["cha_eng_onager"] = {
		culture = "other",
		class = "poor",
		count = 40
	},

	["cha_eng_onager_bastion"] = {
		culture = "other",
		class = "poor",
		count = 4
	},

	["cha_eng_royal_companions"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["cha_eng_royal_horsemen"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["cha_eng_royal_thegns"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["cha_eng_select_archers"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["cha_eng_select_axemen"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["cha_eng_select_spear"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["cha_eng_thegn_spearmen"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["cha_eng_thegns"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["cha_fra_archers"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["cha_fra_armoured_spear"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["cha_fra_armoured_sword"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["cha_fra_artillery_dromon"] = {
		culture = "other",
		class = "poor",
		count = 64
	},

	["cha_fra_castle_dromon_picked_marines"] = {
		culture = "other",
		class = "poor",
		count = 128
	},

	["cha_fra_cav"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["cha_fra_dromon_heavy_marines"] = {
		culture = "other",
		class = "poor",
		count = 128
	},

	["cha_fra_dromon_light_boatmen"] = {
		culture = "other",
		class = "poor",
		count = 88
	},

	["cha_fra_dromon_marines"] = {
		culture = "other",
		class = "poor",
		count = 128
	},

	["cha_fra_guardsmen"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["cha_fra_horsemen"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["cha_fra_large_onager"] = {
		culture = "other",
		class = "poor",
		count = 40
	},

	["cha_fra_levy_archers"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["cha_fra_levy_skirm"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["cha_fra_levy_spear"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["cha_fra_mounted_archers"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["cha_fra_mounted_spear"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["cha_fra_mounted_swordsmen"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["cha_fra_onager"] = {
		culture = "other",
		class = "poor",
		count = 40
	},

	["cha_fra_onager_bastion"] = {
		culture = "other",
		class = "poor",
		count = 4
	},

	["cha_fra_palace_guard"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["cha_fra_paladin_guardsmen"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["cha_fra_scola_cav"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["cha_fra_scola_knights"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["cha_fra_skirm"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["cha_fra_spear"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["cha_fra_sword"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["cha_gen_archers"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["cha_gen_armoured_spear"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["cha_gen_armoured_sword"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["cha_gen_artillery_dromon"] = {
		culture = "other",
		class = "poor",
		count = 64
	},

	["cha_gen_castle_dromon_guard_marines"] = {
		culture = "other",
		class = "poor",
		count = 128
	},

	["cha_gen_dromon_heavy_marines"] = {
		culture = "other",
		class = "poor",
		count = 128
	},

	["cha_gen_dromon_light_boatmen"] = {
		culture = "other",
		class = "poor",
		count = 88
	},

	["cha_gen_dromon_marines"] = {
		culture = "other",
		class = "poor",
		count = 128
	},

	["cha_gen_guard_cav"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["cha_gen_knights"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["cha_gen_large_onager"] = {
		culture = "other",
		class = "poor",
		count = 40
	},

	["cha_gen_levy_archers"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["cha_gen_levy_skirm"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["cha_gen_levy_spear"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["cha_gen_noble_cav"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["cha_gen_onager"] = {
		culture = "other",
		class = "poor",
		count = 40
	},

	["cha_gen_onager_bastion"] = {
		culture = "other",
		class = "poor",
		count = 4
	},

	["cha_gen_skirm"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["cha_gen_skirm_cav"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["cha_gen_spear"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["cha_gen_sword"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["cha_lom_archers"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["cha_lom_artillery_dromon"] = {
		culture = "other",
		class = "poor",
		count = 64
	},

	["cha_lom_axemen"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["cha_lom_castle_dromon_gastald_marines"] = {
		culture = "other",
		class = "poor",
		count = 128
	},

	["cha_lom_clubmen"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["cha_lom_dromon_heavy_marines"] = {
		culture = "other",
		class = "poor",
		count = 128
	},

	["cha_lom_dromon_light_boatmen"] = {
		culture = "other",
		class = "poor",
		count = 88
	},

	["cha_lom_dromon_marines"] = {
		culture = "other",
		class = "poor",
		count = 128
	},

	["cha_lom_gastald_cav"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["cha_lom_gastald_guardsmen"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["cha_lom_gastald_knights"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["cha_lom_gastald_swordsmen"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["cha_lom_guardsmen"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["cha_lom_horsemen"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["cha_lom_lancers"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["cha_lom_large_onager"] = {
		culture = "other",
		class = "poor",
		count = 40
	},

	["cha_lom_levy"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["cha_lom_levy_archers"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["cha_lom_levy_axemen"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["cha_lom_levy_skirm"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["cha_lom_levy_spear"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["cha_lom_night_raiders"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["cha_lom_onager"] = {
		culture = "other",
		class = "poor",
		count = 40
	},

	["cha_lom_onager_bastion"] = {
		culture = "other",
		class = "poor",
		count = 4
	},

	["cha_lom_raider_cav"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["cha_lom_skirm"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["cha_lom_spear"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["cha_lom_sword"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["cha_merc_avar_horse_archers"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["cha_merc_avar_lancers"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["cha_merc_avar_longship_boatmen"] = {
		culture = "other",
		class = "poor",
		count = 128
	},

	["cha_merc_avar_longship_light_boatmen"] = {
		culture = "other",
		class = "poor",
		count = 88
	},

	["cha_merc_bre_armorican_cav"] = {
		culture = "other",
		class = "med",
		count = 80
	},

	["cha_merc_byz_archers"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["cha_merc_byz_dromon_archer_boatmen"] = {
		culture = "other",
		class = "poor",
		count = 88
	},

	["cha_merc_byz_dromon_levy_marines"] = {
		culture = "other",
		class = "poor",
		count = 128
	},

	["cha_merc_byz_dromon_skutatoi"] = {
		culture = "other",
		class = "poor",
		count = 128
	},

	["cha_merc_byz_light_cav"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["cha_merc_byz_skutatoi"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["cha_merc_cel_armoured_spear"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["cha_merc_cel_fianna"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["cha_merc_cel_horsemen"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["cha_merc_cel_kerns"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["cha_merc_cel_long_fhada_archer_boatmen"] = {
		culture = "other",
		class = "poor",
		count = 88
	},

	["cha_merc_cel_long_fhada_javelin_boatmen"] = {
		culture = "other",
		class = "poor",
		count = 88
	},

	["cha_merc_cel_long_fhada_spear_boatmen"] = {
		culture = "other",
		class = "poor",
		count = 128
	},

	["cha_merc_cel_longship_archer_boatmen"] = {
		culture = "other",
		class = "poor",
		count = 88
	},

	["cha_merc_cel_longship_javelin_boatmen"] = {
		culture = "other",
		class = "poor",
		count = 88
	},

	["cha_merc_cel_longship_spear_boatmen"] = {
		culture = "other",
		class = "poor",
		count = 128
	},

	["cha_merc_cel_pictish_crossbowmen"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["cha_merc_cel_sword_band"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["cha_merc_cel_welsh_archers"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["cha_merc_eng_fyrd_archers"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["cha_merc_eng_fyrd_spear"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["cha_merc_eng_horsemen"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["cha_merc_eng_longship_fyrd_archer_boatmen"] = {
		culture = "other",
		class = "poor",
		count = 88
	},

	["cha_merc_eng_longship_fyrd_boatmen"] = {
		culture = "other",
		class = "poor",
		count = 128
	},

	["cha_merc_eng_thegns"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["cha_merc_fra_archers"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["cha_merc_fra_armoured_sword"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["cha_merc_fra_dromon_light_boatmen"] = {
		culture = "other",
		class = "poor",
		count = 88
	},

	["cha_merc_fra_dromon_marines"] = {
		culture = "other",
		class = "poor",
		count = 128
	},

	["cha_merc_fra_horsemen"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["cha_merc_fra_spear"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["cha_merc_gen_archers"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["cha_merc_gen_armoured_spear"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["cha_merc_gen_armoured_sword"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["cha_merc_gen_dromon_light_boatmen"] = {
		culture = "other",
		class = "poor",
		count = 88
	},

	["cha_merc_gen_dromon_marines"] = {
		culture = "other",
		class = "poor",
		count = 128
	},

	["cha_merc_gen_skirm"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["cha_merc_gen_skirm_cav"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["cha_merc_gen_spear"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["cha_merc_lom_archers"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["cha_merc_lom_clubmen"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["cha_merc_lom_dromon_light_boatmen"] = {
		culture = "other",
		class = "poor",
		count = 88
	},

	["cha_merc_lom_dromon_marines"] = {
		culture = "other",
		class = "poor",
		count = 128
	},

	["cha_merc_lom_horsemen"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["cha_merc_lom_raider_cav"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["cha_merc_lom_spear"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["cha_merc_mus_andalusian_cav"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["cha_merc_mus_andalusian_infantry"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["cha_merc_mus_andalusian_skirm"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["cha_merc_mus_berber_levy"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["cha_merc_mus_berber_raiders"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["cha_merc_mus_berber_spearmen"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["cha_merc_mus_dromonian_galley_andalusian_marines"] = {
		culture = "other",
		class = "poor",
		count = 128
	},

	["cha_merc_mus_dromonian_galley_archer_boatmen"] = {
		culture = "other",
		class = "poor",
		count = 88
	},

	["cha_merc_sax_javelinmen"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["cha_merc_sax_longship_boatmen"] = {
		culture = "other",
		class = "poor",
		count = 128
	},

	["cha_merc_sax_longship_light_raiders"] = {
		culture = "other",
		class = "poor",
		count = 88
	},

	["cha_merc_sax_longship_raiders"] = {
		culture = "other",
		class = "poor",
		count = 128
	},

	["cha_merc_sax_scout_cav"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["cha_merc_sax_seax_warriors"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["cha_merc_sax_spear_band"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["cha_merc_slav_ambushers"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["cha_merc_slav_archers"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["cha_merc_slav_longship_boatmen"] = {
		culture = "other",
		class = "poor",
		count = 128
	},

	["cha_merc_slav_longship_light_boatmen"] = {
		culture = "other",
		class = "poor",
		count = 88
	},

	["cha_merc_slav_raiders"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["cha_merc_slav_tribesmen"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["cha_merc_spa_ambushers"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["cha_merc_spa_iberian_cav"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["cha_merc_spa_raider_cav"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["cha_merc_vik_archers"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["cha_merc_vik_axe_freemen"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["cha_merc_vik_big_axes"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["cha_merc_vik_longship_raiders"] = {
		culture = "other",
		class = "poor",
		count = 128
	},

	["cha_merc_vik_longship_ship_archers"] = {
		culture = "other",
		class = "poor",
		count = 88
	},

	["cha_merc_vik_longship_smashers"] = {
		culture = "other",
		class = "poor",
		count = 128
	},

	["cha_merc_vik_sword_hirdmen"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["cha_mus_andalusian_cav"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["cha_mus_andalusian_infantry"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["cha_mus_andalusian_skirm"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["cha_mus_andalusian_warriors"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["cha_mus_archers"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["cha_mus_armoured_archers"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["cha_mus_armoured_cav"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["cha_mus_artillery_dromon"] = {
		culture = "other",
		class = "poor",
		count = 64
	},

	["cha_mus_berber_horsemen"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["cha_mus_berber_jinetes"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["cha_mus_berber_levy"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["cha_mus_berber_light_cav"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["cha_mus_berber_raiders"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["cha_mus_berber_spearmen"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["cha_mus_berber_warriors"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["cha_mus_castle_dromon_guard_marines"] = {
		culture = "other",
		class = "poor",
		count = 128
	},

	["cha_mus_dromon_archer_marines"] = {
		culture = "other",
		class = "poor",
		count = 88
	},

	["cha_mus_dromon_armoured_marines"] = {
		culture = "other",
		class = "poor",
		count = 128
	},

	["cha_mus_dromonian_galley_andalusian_marines"] = {
		culture = "other",
		class = "poor",
		count = 128
	},

	["cha_mus_dromonian_galley_archer_boatmen"] = {
		culture = "other",
		class = "poor",
		count = 88
	},

	["cha_mus_large_onager"] = {
		culture = "other",
		class = "poor",
		count = 40
	},

	["cha_mus_onager"] = {
		culture = "other",
		class = "poor",
		count = 40
	},

	["cha_mus_onager_bastion"] = {
		culture = "other",
		class = "poor",
		count = 4
	},

	["cha_mus_slingers"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["cha_mus_umayyad_guard_cav"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["cha_mus_umayyad_guardsmen"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["cha_pap_guardsmen"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["cha_sax_archers"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["cha_sax_axe_band"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["cha_sax_axe_warriors"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["cha_sax_cav"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["cha_sax_dragonship_hearth_marines"] = {
		culture = "other",
		class = "poor",
		count = 128
	},

	["cha_sax_hearth_cav"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["cha_sax_hearth_guard"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["cha_sax_javelinmen"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["cha_sax_levy_archers"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["cha_sax_levy_javelinmen"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["cha_sax_longship_boatmen"] = {
		culture = "other",
		class = "poor",
		count = 128
	},

	["cha_sax_longship_heavy_boatmen"] = {
		culture = "other",
		class = "poor",
		count = 128
	},

	["cha_sax_longship_light_raiders"] = {
		culture = "other",
		class = "poor",
		count = 88
	},

	["cha_sax_longship_raiders"] = {
		culture = "other",
		class = "poor",
		count = 128
	},

	["cha_sax_noble_javelinmen"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["cha_sax_onager"] = {
		culture = "other",
		class = "poor",
		count = 40
	},

	["cha_sax_onager_bastion"] = {
		culture = "other",
		class = "poor",
		count = 4
	},

	["cha_sax_scout_cav"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["cha_sax_seax_band"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["cha_sax_seax_warriors"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["cha_sax_spear_band"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["cha_sax_spear_veterans"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["cha_sax_spear_warriors"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["cha_sax_sword_veterans"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["cha_sax_warhounds"] = {
		culture = "other",
		class = "poor",
		count = 40
	},

	["cha_slav_ambushers"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["cha_slav_archers"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["cha_slav_champions"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["cha_slav_dragonship_elite_boatmen"] = {
		culture = "other",
		class = "poor",
		count = 128
	},

	["cha_slav_large_shields"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["cha_slav_longship_boatmen"] = {
		culture = "other",
		class = "poor",
		count = 128
	},

	["cha_slav_longship_light_boatmen"] = {
		culture = "other",
		class = "poor",
		count = 88
	},

	["cha_slav_onager"] = {
		culture = "other",
		class = "poor",
		count = 40
	},

	["cha_slav_onager_bastion"] = {
		culture = "other",
		class = "poor",
		count = 4
	},

	["cha_slav_raiders"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["cha_slav_royal_cav"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["cha_slav_skirm"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["cha_slav_spear"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["cha_slav_tribesmen"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["cha_slav_warriors"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["cha_spa_ambushers"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["cha_spa_iberian_cav"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["cha_spa_jinetes"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["cha_spa_noble_skirm"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["cha_spa_raider_cav"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["cha_spa_royal_cav"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["cha_spa_royal_guard_cavalry"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["cha_spa_royal_guardsmen"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["cha_spa_royal_knights"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["cha_spa_tribesmen"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["cha_vik_archers"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["cha_vik_armoured_archers"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["cha_vik_axe_freemen"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["cha_vik_axe_hirdmen"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["cha_vik_axemen"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["cha_vik_berserkers"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["cha_vik_big_axes"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["cha_vik_dragonship_housecarl"] = {
		culture = "other",
		class = "poor",
		count = 128
	},

	["cha_vik_dragonship_warlords_companions"] = {
		culture = "other",
		class = "poor",
		count = 128
	},

	["cha_vik_housecarls"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["cha_vik_hunters"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["cha_vik_levy_freemen"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["cha_vik_longship_heavy_raiders"] = {
		culture = "other",
		class = "poor",
		count = 128
	},

	["cha_vik_longship_marauders"] = {
		culture = "other",
		class = "poor",
		count = 128
	},

	["cha_vik_longship_raiders"] = {
		culture = "other",
		class = "poor",
		count = 128
	},

	["cha_vik_longship_ship_archers"] = {
		culture = "other",
		class = "poor",
		count = 88
	},

	["cha_vik_longship_smashers"] = {
		culture = "other",
		class = "poor",
		count = 128
	},

	["cha_vik_onager"] = {
		culture = "other",
		class = "poor",
		count = 40
	},

	["cha_vik_onager_bastion"] = {
		culture = "other",
		class = "poor",
		count = 4
	},

	["cha_vik_skirm"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["cha_vik_spear"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["cha_vik_sword_hirdmen"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["cha_vik_warlords_companions"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["champ_castellans"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["champ_crossbowmen"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["champ_maa"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["champ_mounted_maa"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["champ_mounted_serjeants"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["champ_retinue"] = {
		culture = "other",
		class = "med",
		count = 50
	},

	["champ_spearmen"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["champ_voulgier"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["chat_castellans"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["chat_crossbowmen"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["chat_maa"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["chat_mounted_maa"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["chat_mounted_serjeants"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["chat_retinue"] = {
		culture = "other",
		class = "med",
		count = 50
	},

	["chat_spearmen"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["chat_voulgier"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["chester_archers"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["chester_billmen"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["chester_castellans"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["chester_maa"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["chester_mounted_maa"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["chester_mounted_serjeants"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["chester_retinue"] = {
		culture = "other",
		class = "med",
		count = 50
	},

	["clermont_retinue"] = {
		culture = "other",
		class = "med",
		count = 50
	},

	["clifford_archers"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["clifford_billmen"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["clifford_castellans"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["clifford_maa"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["clifford_mounted_maa"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["clifford_mounted_serjeants"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["clifford_retinue"] = {
		culture = "other",
		class = "med",
		count = 50
	},

	["condo_crossbowmen"] = {
		culture = "other",
		class = "rich",
		count = 120
	},

	["condo_gunners"] = {
		culture = "other",
		class = "rich",
		count = 120
	},

	["condo_swordsmen"] = {
		culture = "other",
		class = "med",
		count = 160
	},

	["cornel_retinue"] = {
		culture = "other",
		class = "med",
		count = 50
	},

	["crossbow_militia"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["crossbowmilitia"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["crossbowsergeants"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["crossbowsergeants2"] = {
		culture = "other",
		class = "med",
		count = 120
	},

	["culverin"] = {
		culture = "other",
		class = "rich",
		count = 50
	},

	["dane_bowmen"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["dane_castle_guards"] = {
		culture = "other",
		class = "med",
		count = 160
	},

	["dane_crossbowmen_early"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["dane_crossbowmen_high"] = {
		culture = "other",
		class = "med",
		count = 120
	},

	["dane_gotland_bowmen"] = {
		culture = "other",
		class = "med",
		count = 120
	},

	["dane_gotland_merchant_footmen"] = {
		culture = "other",
		class = "med",
		count = 160
	},

	["dane_gotland_peasant_militia"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["dane_halberdiers"] = {
		culture = "other",
		class = "rich",
		count = 160
	},

	["dane_handgunners"] = {
		culture = "other",
		class = "rich",
		count = 120
	},

	["dane_huskarls_late"] = {
		culture = "other",
		class = "med",
		count = 160
	},

	["dane_king_bodyguard_early"] = {
		culture = "other",
		class = "med",
		count = 50
	},

	["dane_king_bodyguard_high"] = {
		culture = "other",
		class = "med",
		count = 50
	},

	["dane_king_bodyguard_late"] = {
		culture = "other",
		class = "rich",
		count = 50
	},

	["dane_knights_early"] = {
		culture = "other",
		class = "med",
		count = 80
	},

	["dane_knights_high"] = {
		culture = "other",
		class = "med",
		count = 80
	},

	["dane_knights_late"] = {
		culture = "other",
		class = "rich",
		count = 80
	},

	["dane_landevaern_infantry"] = {
		culture = "other",
		class = "poor",
		count = 180
	},

	["dane_landevaern_spearmen"] = {
		culture = "other",
		class = "poor",
		count = 180
	},

	["dane_longbowmen"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["dane_mounted_crossbowmen"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["dane_mounted_crossbowmen_late"] = {
		culture = "other",
		class = "med",
		count = 80
	},

	["dane_mounted_nobles_high"] = {
		culture = "other",
		class = "rich",
		count = 80
	},

	["dane_mounted_nobles_late"] = {
		culture = "other",
		class = "rich",
		count = 80
	},

	["dane_noble_longswords"] = {
		culture = "other",
		class = "rich",
		count = 160
	},

	["dane_pavise_crossbowmen"] = {
		culture = "other",
		class = "rich",
		count = 120
	},

	["dane_pavise_spearmen"] = {
		culture = "other",
		class = "rich",
		count = 160
	},

	["dane_pikemen_high"] = {
		culture = "other",
		class = "med",
		count = 160
	},

	["dane_pikemen_late"] = {
		culture = "other",
		class = "rich",
		count = 160
	},

	["dane_scouts"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["dane_serjeants_early"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["dane_serjeants_high"] = {
		culture = "other",
		class = "med",
		count = 160
	},

	["dane_spear_serjeants_early"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["dane_spear_serjeants_high"] = {
		culture = "other",
		class = "med",
		count = 160
	},

	["dauphine_retinue"] = {
		culture = "other",
		class = "med",
		count = 50
	},

	["dreux_castellans"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["dreux_crossbowmen"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["dreux_maa"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["dreux_mounted_maa"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["dreux_mounted_serjeants"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["dreux_retinue"] = {
		culture = "other",
		class = "med",
		count = 50
	},

	["dreux_spearmen"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["dreux_voulgier"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["eng_archer_militia"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["eng_handgunners"] = {
		culture = "other",
		class = "rich",
		count = 120
	},

	["eng_spear_militia"] = {
		culture = "other",
		class = "poor",
		count = 180
	},

	["english_archer"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["english_castellans"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["english_late_bodyguard"] = {
		culture = "other",
		class = "rich",
		count = 50
	},

	["english_late_retinue"] = {
		culture = "other",
		class = "rich",
		count = 50
	},

	["english_late_serjeants"] = {
		culture = "other",
		class = "med",
		count = 160
	},

	["english_maa"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["english_mid_billmen"] = {
		culture = "other",
		class = "med",
		count = 160
	},

	["english_mid_castellans"] = {
		culture = "other",
		class = "med",
		count = 160
	},

	["english_mid_knights"] = {
		culture = "other",
		class = "med",
		count = 80
	},

	["english_mid_mounted_serjeants"] = {
		culture = "other",
		class = "rich",
		count = 80
	},

	["english_mounted_maa"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["english_mounted_serjeants"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["english_mounted_serjeants_late"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["english_retinue"] = {
		culture = "other",
		class = "med",
		count = 50
	},

	["english_soldier"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["english_yeomen_archers"] = {
		culture = "other",
		class = "med",
		count = 120
	},

	["erill_retinue"] = {
		culture = "other",
		class = "med",
		count = 50
	},

	["feno_retinue"] = {
		culture = "other",
		class = "med",
		count = 50
	},

	["forez_castellans"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["forez_crossbowmen"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["forez_maa"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["forez_mounted_maa"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["forez_mounted_serjeants"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["forez_retinue"] = {
		culture = "other",
		class = "med",
		count = 50
	},

	["forez_spearmen"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["forez_voulgier"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["fre_late_serjeants"] = {
		culture = "other",
		class = "med",
		count = 160
	},

	["fre_scouts"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["fre_spear_militia"] = {
		culture = "other",
		class = "poor",
		count = 180
	},

	["french_castellans"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["french_crossbowmen"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["french_handgunners"] = {
		culture = "other",
		class = "rich",
		count = 120
	},

	["french_late_bodyguard"] = {
		culture = "other",
		class = "rich",
		count = 50
	},

	["french_late_retinue"] = {
		culture = "other",
		class = "rich",
		count = 50
	},

	["french_maa"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["french_mid_castellans"] = {
		culture = "other",
		class = "med",
		count = 160
	},

	["french_mid_crossbowmen"] = {
		culture = "other",
		class = "med",
		count = 120
	},

	["french_mid_knights"] = {
		culture = "other",
		class = "med",
		count = 80
	},

	["french_mid_mounted_serjeants"] = {
		culture = "other",
		class = "rich",
		count = 80
	},

	["french_mid_spearmen"] = {
		culture = "other",
		class = "med",
		count = 160
	},

	["french_mid_voulgier"] = {
		culture = "other",
		class = "med",
		count = 160
	},

	["french_mounted_crossbowmen"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["french_mounted_crossbowmen_high"] = {
		culture = "other",
		class = "med",
		count = 80
	},

	["french_mounted_crossbowmen_late"] = {
		culture = "other",
		class = "med",
		count = 80
	},

	["french_mounted_maa"] = {
		culture = "other",
		class = "med",
		count = 80
	},

	["french_mounted_serjeants"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["french_mounted_serjeants_late"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["french_pavisiers"] = {
		culture = "other",
		class = "rich",
		count = 160
	},

	["french_retinue"] = {
		culture = "other",
		class = "med",
		count = 50
	},

	["french_spearmen"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["french_voulgier"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["german_mid_spearmen"] = {
		culture = "other",
		class = "med",
		count = 160
	},

	["german_mid_swordsmen"] = {
		culture = "other",
		class = "med",
		count = 160
	},

	["grendarmes"] = {
		culture = "other",
		class = "rich",
		count = 80
	},

	["gudal_retinue"] = {
		culture = "other",
		class = "med",
		count = 50
	},

	["halberdiers"] = {
		culture = "other",
		class = "rich",
		count = 160
	},

	["han_t2_hanse_crossbowmen"] = {
		culture = "other",
		class = "med",
		count = 120
	},

	["han_t2_hanse_lance"] = {
		culture = "other",
		class = "rich",
		count = 80
	},

	["heavy_billmen"] = {
		culture = "other",
		class = "rich",
		count = 160
	},

	["heavy_crossbowmen"] = {
		culture = "other",
		class = "rich",
		count = 120
	},

	["heavy_voulgier"] = {
		culture = "other",
		class = "rich",
		count = 160
	},

	["hereford_archers"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["hereford_billmen"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["hereford_castellans"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["hereford_maa"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["hereford_mounted_maa"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["hereford_mounted_serjeants"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["hereford_retinue"] = {
		culture = "other",
		class = "med",
		count = 50
	},

	["hertford_archers"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["hertford_billmen"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["hertford_castellans"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["hertford_maa"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["hertford_mounted_maa"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["hertford_mounted_serjeants"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["hertford_retinue"] = {
		culture = "other",
		class = "med",
		count = 50
	},

	["high_maa"] = {
		culture = "other",
		class = "rich",
		count = 160
	},

	["high_mounted_maa"] = {
		culture = "other",
		class = "rich",
		count = 80
	},

	["hobilars"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["hre_late_serjeants"] = {
		culture = "other",
		class = "med",
		count = 160
	},

	["hre_pavise_crossbowmen"] = {
		culture = "other",
		class = "rich",
		count = 120
	},

	["hre_pavisiers"] = {
		culture = "other",
		class = "rich",
		count = 160
	},

	["hun_handgunners"] = {
		culture = "other",
		class = "rich",
		count = 120
	},

	["jau_retinue"] = {
		culture = "other",
		class = "med",
		count = 50
	},

	["jinetes"] = {
		culture = "other",
		class = "med",
		count = 80
	},

	["knights_tier2"] = {
		culture = "other",
		class = "med",
		count = 80
	},

	["kyburgaxemen"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["kyburgerfootknights"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["kyburgknights_mounted"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["kyburgnotables_mounted"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["lat_bow_militia"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["lat_crossbow_early_mounted"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["lat_crossbow_maa"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["lat_crossbow_militia"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["lat_early_chevaliers"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["lat_early_pikes"] = {
		culture = "other",
		class = "poor",
		count = 180
	},

	["lat_early_serjeants"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["lat_flanders_bodyguard"] = {
		culture = "other",
		class = "med",
		count = 50
	},

	["lat_foot_gendarmes"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["lat_foot_serjeants"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["lat_late_chevaliers"] = {
		culture = "other",
		class = "rich",
		count = 80
	},

	["lat_late_halberdiers"] = {
		culture = "other",
		class = "rich",
		count = 160
	},

	["lat_late_handguns"] = {
		culture = "other",
		class = "rich",
		count = 120
	},

	["lat_late_pavise_crossbow"] = {
		culture = "other",
		class = "rich",
		count = 120
	},

	["lat_late_pike"] = {
		culture = "other",
		class = "rich",
		count = 160
	},

	["lat_late_sword_militia"] = {
		culture = "other",
		class = "med",
		count = 160
	},

	["lat_spear_militia"] = {
		culture = "other",
		class = "poor",
		count = 180
	},

	["lat_t2_archer_milice"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["lat_t2_chevaliers"] = {
		culture = "other",
		class = "med",
		count = 80
	},

	["lat_t2_crossbow_milice"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["lat_t2_garde_du_corps"] = {
		culture = "other",
		class = "med",
		count = 50
	},

	["lat_t2_handgunner"] = {
		culture = "other",
		class = "rich",
		count = 120
	},

	["lat_t2_hommes_d'armes"] = {
		culture = "other",
		class = "rich",
		count = 160
	},

	["lat_t2_mounted_crossbow"] = {
		culture = "other",
		class = "med",
		count = 80
	},

	["lat_t2_mounted_sergents"] = {
		culture = "other",
		class = "rich",
		count = 80
	},

	["lat_t2_pike_milice"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["lat_t2_sergents"] = {
		culture = "other",
		class = "med",
		count = 120
	},

	["lat_t2_spear_milice"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["lincoln_archers"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["lincoln_billmen"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["lincoln_castellans"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["lincoln_maa"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["lincoln_mounted_maa"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["lincoln_mounted_serjeants"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["lincoln_retinue"] = {
		culture = "other",
		class = "med",
		count = 50
	},

	["ltd_armenian_HA"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["ltd_armenian_azat_archers"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["ltd_armenian_heavy_cavalry"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["ltd_armenian_heavy_spearmen"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["ltd_armenian_highlanders"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["ltd_armenian_infantry"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["ltd_armenian_levy_archers"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["ltd_armenian_light_spearmen"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["ltd_armenian_mounted_serjeants"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["ltd_armenian_nakharar_guard"] = {
		culture = "other",
		class = "med",
		count = 50
	},

	["ltd_armenian_serjeant_crossbowmen"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["ltd_armenian_spearmen"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["ltd_ayyubid_ahdath"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["ltd_ayyubid_ahdath_archers"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["ltd_ayyubid_ajnad_cavalry"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["ltd_ayyubid_askari_cavalry"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["ltd_ayyubid_askari_nobles"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["ltd_ayyubid_beduin_camel_riders"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["ltd_ayyubid_beja_tribal_spearmen"] = {
		culture = "other",
		class = "poor",
		count = 180
	},

	["ltd_ayyubid_crossbowmen"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["ltd_ayyubid_harafisha_infantry"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["ltd_ayyubid_jund_spearmen"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["ltd_ayyubid_jund_swordsmen"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["ltd_ayyubid_mamluk_tabardariyah"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["ltd_ayyubid_naffatun"] = {
		culture = "other",
		class = "med",
		count = 50
	},

	["ltd_ayyubid_nubian_spearmen"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["ltd_ayyubid_sudanese_archers"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["ltd_ayyubid_sultans_bodyguard"] = {
		culture = "other",
		class = "med",
		count = 50
	},

	["ltd_ayyubid_thaqlah_axemen"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["ltd_bohemian_HUSSITE_bodyguard"] = {
		culture = "other",
		class = "rich",
		count = 50
	},

	["ltd_bohemian_HUSSITE_bodyguard_2"] = {
		culture = "other",
		class = "rich",
		count = 50
	},

	["ltd_bohemian_HUSSITE_bodyguard_taborite"] = {
		culture = "other",
		class = "med",
		count = 50
	},

	["ltd_bohemian_HUSSITE_flailmen"] = {
		culture = "other",
		class = "rich",
		count = 160
	},

	["ltd_bohemian_HUSSITE_halberdiers"] = {
		culture = "other",
		class = "med",
		count = 160
	},

	["ltd_bohemian_HUSSITE_handgunners"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["ltd_bohemian_HUSSITE_late_mounted_cossbowmen"] = {
		culture = "other",
		class = "med",
		count = 80
	},

	["ltd_bohemian_HUSSITE_light_cavalry"] = {
		culture = "other",
		class = "rich",
		count = 80
	},

	["ltd_bohemian_HUSSITE_pavise_crossbowmen"] = {
		culture = "other",
		class = "rich",
		count = 120
	},

	["ltd_bohemian_HUSSITE_pavise_spearmen"] = {
		culture = "other",
		class = "med",
		count = 160
	},

	["ltd_bohemian_MORAVIAN_bodyguard_high"] = {
		culture = "other",
		class = "med",
		count = 50
	},

	["ltd_bohemian_archers"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["ltd_bohemian_archers_high"] = {
		culture = "other",
		class = "med",
		count = 120
	},

	["ltd_bohemian_archers_late"] = {
		culture = "other",
		class = "rich",
		count = 120
	},

	["ltd_bohemian_armored_halberdiers_high"] = {
		culture = "other",
		class = "med",
		count = 160
	},

	["ltd_bohemian_chod_frontiersmen"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["ltd_bohemian_halberdiers_late"] = {
		culture = "other",
		class = "rich",
		count = 160
	},

	["ltd_bohemian_handgunners"] = {
		culture = "other",
		class = "rich",
		count = 120
	},

	["ltd_bohemian_king_bodyguard_early"] = {
		culture = "other",
		class = "med",
		count = 50
	},

	["ltd_bohemian_king_bodyguard_high"] = {
		culture = "other",
		class = "rich",
		count = 50
	},

	["ltd_bohemian_king_bodyguard_late"] = {
		culture = "other",
		class = "med",
		count = 50
	},

	["ltd_bohemian_knight_high"] = {
		culture = "other",
		class = "rich",
		count = 80
	},

	["ltd_bohemian_knights_early"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["ltd_bohemian_knights_late"] = {
		culture = "other",
		class = "rich",
		count = 80
	},

	["ltd_bohemian_late_mounted_crossbowmen"] = {
		culture = "other",
		class = "med",
		count = 80
	},

	["ltd_bohemian_macemen_late"] = {
		culture = "other",
		class = "med",
		count = 160
	},

	["ltd_bohemian_militia_archers"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["ltd_bohemian_militia_halberdiers_high"] = {
		culture = "other",
		class = "poor",
		count = 180
	},

	["ltd_bohemian_mounted_raiders"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["ltd_bohemian_mounted_sergeants"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["ltd_bohemian_pacholek_spearmen"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["ltd_bohemian_pavise_crossbowmen_late"] = {
		culture = "other",
		class = "rich",
		count = 120
	},

	["ltd_bohemian_pavise_spearmen_high"] = {
		culture = "other",
		class = "rich",
		count = 160
	},

	["ltd_bohemian_pavise_spearmen_late"] = {
		culture = "other",
		class = "rich",
		count = 160
	},

	["ltd_bohemian_peasant_archers"] = {
		culture = "other",
		class = "poor",
		count = 180
	},

	["ltd_bohemian_sergeant_spearmen"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["ltd_bohemian_sergeants"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["ltd_bulg_HA"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["ltd_bulg_archers"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["ltd_bulg_auxiliary"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["ltd_bulg_axemen"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["ltd_bulg_elite_archers"] = {
		culture = "other",
		class = "med",
		count = 120
	},

	["ltd_bulg_heavy_spearmen"] = {
		culture = "other",
		class = "med",
		count = 160
	},

	["ltd_bulg_hooked_pikemen"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["ltd_bulg_late_bolyars"] = {
		culture = "other",
		class = "med",
		count = 80
	},

	["ltd_bulg_late_crossbowmen"] = {
		culture = "other",
		class = "med",
		count = 120
	},

	["ltd_bulg_late_infantry"] = {
		culture = "other",
		class = "med",
		count = 160
	},

	["ltd_bulg_militia"] = {
		culture = "other",
		class = "poor",
		count = 180
	},

	["ltd_bulg_noble_HA"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["ltd_bulg_noble_cavalry"] = {
		culture = "other",
		class = "med",
		count = 50
	},

	["ltd_bulg_pronoia_lancer"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["ltd_bulg_spearmen"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["ltd_bulg_swordsmen"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["ltd_byz_akritai_high"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["ltd_byz_bodyguard"] = {
		culture = "other",
		class = "rich",
		count = 50
	},

	["ltd_byz_cataphractoi"] = {
		culture = "other",
		class = "rich",
		count = 80
	},

	["ltd_byz_emperor_bodyguard_high"] = {
		culture = "other",
		class = "rich",
		count = 50
	},

	["ltd_byz_emperor_bodyguard_late"] = {
		culture = "other",
		class = "med",
		count = 50
	},

	["ltd_byz_gasmouloi"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["ltd_byz_heavy_archers_high"] = {
		culture = "other",
		class = "med",
		count = 120
	},

	["ltd_byz_heavy_infantry_high"] = {
		culture = "other",
		class = "med",
		count = 160
	},

	["ltd_byz_heavy_infantry_late"] = {
		culture = "other",
		class = "rich",
		count = 160
	},

	["ltd_byz_heir_bodyguard"] = {
		culture = "other",
		class = "rich",
		count = 50
	},

	["ltd_byz_kavallarioi"] = {
		culture = "other",
		class = "med",
		count = 80
	},

	["ltd_byz_kavallarioi_high"] = {
		culture = "other",
		class = "rich",
		count = 80
	},

	["ltd_byz_kontaratoi"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["ltd_byz_latinikon"] = {
		culture = "other",
		class = "rich",
		count = 80
	},

	["ltd_byz_militia"] = {
		culture = "other",
		class = "poor",
		count = 180
	},

	["ltd_byz_mounted_akritai_high"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["ltd_byz_mourtatoi"] = {
		culture = "other",
		class = "rich",
		count = 120
	},

	["ltd_byz_peltastoi"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["ltd_byz_pronoiaroi"] = {
		culture = "other",
		class = "rich",
		count = 80
	},

	["ltd_byz_rus_mercenaries"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["ltd_byz_scoutatoi"] = {
		culture = "other",
		class = "med",
		count = 160
	},

	["ltd_byz_scoutatoi_high"] = {
		culture = "other",
		class = "med",
		count = 160
	},

	["ltd_byz_scoutatoi_swordsmen"] = {
		culture = "other",
		class = "med",
		count = 160
	},

	["ltd_byz_scoutatoi_swordsmen_high"] = {
		culture = "other",
		class = "med",
		count = 160
	},

	["ltd_byz_spatharioi_guard"] = {
		culture = "other",
		class = "rich",
		count = 160
	},

	["ltd_byz_turcoman_HA"] = {
		culture = "other",
		class = "med",
		count = 80
	},

	["ltd_catalan_company"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["ltd_cuman_archer"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["ltd_cuman_axemen"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["ltd_cuman_heavy_HA"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["ltd_cuman_heavy_lancers"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["ltd_cuman_heavy_spearmen"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["ltd_cuman_horse_skirm"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["ltd_cuman_javelinmen"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["ltd_cuman_khans_lancers"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["ltd_cuman_light_HA"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["ltd_cuman_macemen_high"] = {
		culture = "other",
		class = "med",
		count = 160
	},

	["ltd_cuman_noble_HA"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["ltd_cuman_nomadic_spearmen"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["ltd_cuman_slav_spearmen"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["ltd_cuman_steppe_lancers"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["ltd_cuman_tribesmen"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["ltd_eastern_euro_peasant_archers"] = {
		culture = "other",
		class = "poor",
		count = 180
	},

	["ltd_eastern_euro_peasants"] = {
		culture = "other",
		class = "poor",
		count = 200
	},

	["ltd_epirus_AOR_german_knights"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["ltd_epirus_emperor_bodyguard"] = {
		culture = "other",
		class = "rich",
		count = 50
	},

	["ltd_epirus_epirote_guard"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["ltd_epirus_epirote_swordsmen"] = {
		culture = "other",
		class = "med",
		count = 160
	},

	["ltd_epirus_heavy_archers"] = {
		culture = "other",
		class = "rich",
		count = 120
	},

	["ltd_epirus_kavallarioi"] = {
		culture = "other",
		class = "med",
		count = 80
	},

	["ltd_epirus_militia_spearmen"] = {
		culture = "other",
		class = "poor",
		count = 180
	},

	["ltd_epirus_scoutatoi"] = {
		culture = "other",
		class = "med",
		count = 160
	},

	["ltd_epirus_scouts"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["ltd_epirus_skirmishers"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["ltd_georg_HA"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["ltd_georg_aznauri_HA"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["ltd_georg_aznauri_lancers"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["ltd_georg_dismounted_monaspa_archers"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["ltd_georg_kartlian_spearmen"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["ltd_georg_khevsur_swordsmen"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["ltd_georg_metsikhovne_archers"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["ltd_georg_metsikhovne_spearmen"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["ltd_georg_monaspa_guard"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["ltd_georg_monaspa_lancers"] = {
		culture = "other",
		class = "rich",
		count = 80
	},

	["ltd_georg_shubosani_molashqre"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["ltd_georg_svanian_crossbowmen"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["ltd_georg_svanian_hunters"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["ltd_georg_tadzreuli"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["ltd_georg_tadzreuli_swordsmen"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["ltd_german_armored_sergeants"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["ltd_german_armored_sergeants_high"] = {
		culture = "other",
		class = "med",
		count = 160
	},

	["ltd_german_armored_sergeants_mounted"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["ltd_german_armored_sergeants_mounted_high"] = {
		culture = "other",
		class = "rich",
		count = 80
	},

	["ltd_german_dismounted_MAA_late_BAVARIA"] = {
		culture = "other",
		class = "rich",
		count = 160
	},

	["ltd_german_dismounted_MAA_late_BRABANT"] = {
		culture = "other",
		class = "rich",
		count = 160
	},

	["ltd_german_dismounted_MAA_late_HRE"] = {
		culture = "other",
		class = "rich",
		count = 160
	},

	["ltd_german_duke_bodyguard_early_BAVARIA"] = {
		culture = "other",
		class = "med",
		count = 50
	},

	["ltd_german_duke_bodyguard_early_BRABANT"] = {
		culture = "other",
		class = "med",
		count = 50
	},

	["ltd_german_duke_bodyguard_high_BAVARIA"] = {
		culture = "other",
		class = "med",
		count = 50
	},

	["ltd_german_duke_bodyguard_high_BRABANT"] = {
		culture = "other",
		class = "med",
		count = 50
	},

	["ltd_german_duke_bodyguard_late_BAVARIA"] = {
		culture = "other",
		class = "rich",
		count = 50
	},

	["ltd_german_duke_bodyguard_late_BRABANT"] = {
		culture = "other",
		class = "rich",
		count = 50
	},

	["ltd_german_emperor_bodyguard_HRE"] = {
		culture = "other",
		class = "med",
		count = 50
	},

	["ltd_german_emperor_bodyguard_high_HRE"] = {
		culture = "other",
		class = "rich",
		count = 50
	},

	["ltd_german_emperor_bodyguard_late_HRE"] = {
		culture = "other",
		class = "med",
		count = 50
	},

	["ltd_german_handgunners"] = {
		culture = "other",
		class = "rich",
		count = 120
	},

	["ltd_german_heavy_crossbowmen"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["ltd_german_imperial_knights_BAVARIA"] = {
		culture = "other",
		class = "rich",
		count = 80
	},

	["ltd_german_imperial_knights_BRABANT"] = {
		culture = "other",
		class = "rich",
		count = 80
	},

	["ltd_german_imperial_knights_HRE"] = {
		culture = "other",
		class = "rich",
		count = 80
	},

	["ltd_german_knight_BAVARIA_early"] = {
		culture = "other",
		class = "med",
		count = 80
	},

	["ltd_german_knight_BRABANT_early"] = {
		culture = "other",
		class = "med",
		count = 80
	},

	["ltd_german_knight_HRE_early"] = {
		culture = "other",
		class = "med",
		count = 80
	},

	["ltd_german_knights_late_BAVARIA"] = {
		culture = "other",
		class = "rich",
		count = 80
	},

	["ltd_german_knights_late_BRABANT"] = {
		culture = "other",
		class = "rich",
		count = 80
	},

	["ltd_german_knights_late_HRE"] = {
		culture = "other",
		class = "rich",
		count = 80
	},

	["ltd_german_mounted_crossbowmen_early"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["ltd_german_mounted_crossbowmen_high"] = {
		culture = "other",
		class = "med",
		count = 80
	},

	["ltd_german_mounted_crossbowmen_late"] = {
		culture = "other",
		class = "med",
		count = 80
	},

	["ltd_german_peasant_crossbowmen"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["ltd_german_pikemen_BRABANT_early"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["ltd_german_pikemen_BRABANT_late"] = {
		culture = "other",
		class = "rich",
		count = 160
	},

	["ltd_german_pikemen_high"] = {
		culture = "other",
		class = "med",
		count = 160
	},

	["ltd_german_sergeant_crossbowmen_high"] = {
		culture = "other",
		class = "med",
		count = 120
	},

	["ltd_german_sergeant_spearmen"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["ltd_german_sergeant_swordsmen"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["ltd_german_urban_militia"] = {
		culture = "other",
		class = "poor",
		count = 180
	},

	["ltd_hun_crossbowmen"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["ltd_hun_halberdiers"] = {
		culture = "other",
		class = "rich",
		count = 160
	},

	["ltd_hun_heavy_crossbowmen"] = {
		culture = "other",
		class = "med",
		count = 120
	},

	["ltd_hun_king_bodyguard"] = {
		culture = "other",
		class = "med",
		count = 50
	},

	["ltd_hun_king_bodyguard_high"] = {
		culture = "other",
		class = "rich",
		count = 50
	},

	["ltd_hun_king_bodyguard_late"] = {
		culture = "other",
		class = "med",
		count = 50
	},

	["ltd_hun_king_bodyguard_mathias_rex"] = {
		culture = "other",
		class = "med",
		count = 50
	},

	["ltd_hun_knight"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["ltd_hun_knight_high"] = {
		culture = "other",
		class = "rich",
		count = 80
	},

	["ltd_hun_knight_late"] = {
		culture = "other",
		class = "rich",
		count = 80
	},

	["ltd_hun_light_lancer"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["ltd_hun_militia_crossbowmen"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["ltd_hun_militia_spear"] = {
		culture = "other",
		class = "poor",
		count = 180
	},

	["ltd_hun_militia_spear_late"] = {
		culture = "other",
		class = "med",
		count = 160
	},

	["ltd_hun_pavise_crossbowmen"] = {
		culture = "other",
		class = "rich",
		count = 120
	},

	["ltd_hun_pavise_crossbowmen_late"] = {
		culture = "other",
		class = "rich",
		count = 120
	},

	["ltd_hun_pavise_spearmen"] = {
		culture = "other",
		class = "rich",
		count = 160
	},

	["ltd_hun_pavise_spearmen_upgrade"] = {
		culture = "other",
		class = "rich",
		count = 160
	},

	["ltd_hun_royal_warriors"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["ltd_hun_spearmen"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["ltd_hun_sword"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["ltd_hun_sword_upgrade"] = {
		culture = "other",
		class = "med",
		count = 160
	},

	["ltd_hun_szekely_HA"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["ltd_lithuanian_armored_cavalry"] = {
		culture = "other",
		class = "rich",
		count = 80
	},

	["ltd_lithuanian_armored_crossbowmen"] = {
		culture = "other",
		class = "med",
		count = 120
	},

	["ltd_lithuanian_armored_infantry"] = {
		culture = "other",
		class = "med",
		count = 160
	},

	["ltd_lithuanian_armored_spearmen"] = {
		culture = "other",
		class = "med",
		count = 160
	},

	["ltd_lithuanian_axemen"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["ltd_lithuanian_baltic_spearmen"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["ltd_lithuanian_boyars"] = {
		culture = "other",
		class = "med",
		count = 50
	},

	["ltd_lithuanian_cavalry"] = {
		culture = "other",
		class = "med",
		count = 80
	},

	["ltd_lithuanian_ducal_axemen"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["ltd_lithuanian_grand_duke_bodyguard_early"] = {
		culture = "other",
		class = "med",
		count = 50
	},

	["ltd_lithuanian_grand_duke_bodyguard_late"] = {
		culture = "other",
		class = "rich",
		count = 50
	},

	["ltd_lithuanian_noble_cavalry"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["ltd_lithuanian_noble_swordsmen"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["ltd_lithuanian_ruthenian_archers"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["ltd_lithuanian_skirmishers"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["ltd_merc_genoese_crossbowmen_late"] = {
		culture = "other",
		class = "rich",
		count = 120
	},

	["ltd_merc_german_knights_late"] = {
		culture = "other",
		class = "rich",
		count = 80
	},

	["ltd_merc_italian_knights_late"] = {
		culture = "other",
		class = "rich",
		count = 80
	},

	["ltd_merc_italian_mercenary_late"] = {
		culture = "other",
		class = "rich",
		count = 160
	},

	["ltd_merc_mazovian_cavalry"] = {
		culture = "other",
		class = "rich",
		count = 80
	},

	["ltd_merc_mazovian_cavalry_high"] = {
		culture = "other",
		class = "rich",
		count = 80
	},

	["ltd_merc_swiss_halberdiers"] = {
		culture = "other",
		class = "rich",
		count = 160
	},

	["ltd_merc_swiss_pikemen"] = {
		culture = "other",
		class = "rich",
		count = 160
	},

	["ltd_merc_transitional_MAA"] = {
		culture = "other",
		class = "rich",
		count = 160
	},

	["ltd_moldavian_warrior"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["ltd_pol_archers_greater_poland"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["ltd_pol_archers_lesser_poland"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["ltd_pol_archers_pomerania"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["ltd_pol_axemen_lesser_poland"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["ltd_pol_axemen_pomerania"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["ltd_pol_great_banner_of_cracow"] = {
		culture = "other",
		class = "rich",
		count = 80
	},

	["ltd_pol_halberdier_high"] = {
		culture = "other",
		class = "med",
		count = 160
	},

	["ltd_pol_halberdier_late"] = {
		culture = "other",
		class = "rich",
		count = 160
	},

	["ltd_pol_handgunners_late"] = {
		culture = "other",
		class = "rich",
		count = 120
	},

	["ltd_pol_heavy_axemen_pomerania"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["ltd_pol_heavy_crossbowmen_high"] = {
		culture = "other",
		class = "med",
		count = 120
	},

	["ltd_pol_heavy_crossbowmen_silesia_high"] = {
		culture = "other",
		class = "med",
		count = 120
	},

	["ltd_pol_high_axemen"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["ltd_pol_king_bodyguard_greater_poland"] = {
		culture = "other",
		class = "med",
		count = 50
	},

	["ltd_pol_king_bodyguard_high"] = {
		culture = "other",
		class = "rich",
		count = 50
	},

	["ltd_pol_king_bodyguard_late"] = {
		culture = "other",
		class = "med",
		count = 50
	},

	["ltd_pol_king_bodyguard_lesser_poland"] = {
		culture = "other",
		class = "med",
		count = 50
	},

	["ltd_pol_king_bodyguard_pomerania"] = {
		culture = "other",
		class = "med",
		count = 50
	},

	["ltd_pol_king_bodyguard_silesia"] = {
		culture = "other",
		class = "med",
		count = 50
	},

	["ltd_pol_knight_greater_poland"] = {
		culture = "other",
		class = "med",
		count = 80
	},

	["ltd_pol_knight_high"] = {
		culture = "other",
		class = "rich",
		count = 80
	},

	["ltd_pol_knight_late"] = {
		culture = "other",
		class = "rich",
		count = 80
	},

	["ltd_pol_knight_lesser_poland"] = {
		culture = "other",
		class = "med",
		count = 80
	},

	["ltd_pol_knight_pomerania"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["ltd_pol_knight_silesia"] = {
		culture = "other",
		class = "med",
		count = 80
	},

	["ltd_pol_knight_silesia_high"] = {
		culture = "other",
		class = "rich",
		count = 80
	},

	["ltd_pol_late_infantry"] = {
		culture = "other",
		class = "med",
		count = 160
	},

	["ltd_pol_militia_crossbowmen"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["ltd_pol_militia_spearmen"] = {
		culture = "other",
		class = "poor",
		count = 180
	},

	["ltd_pol_mounted_crossbowmen_high"] = {
		culture = "other",
		class = "med",
		count = 80
	},

	["ltd_pol_mounted_crossbowmen_late"] = {
		culture = "other",
		class = "med",
		count = 80
	},

	["ltd_pol_mounted_crossbowmen_silesia"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["ltd_pol_mounted_sergeants"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["ltd_pol_mounted_sergeants_silesia"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["ltd_pol_pavise_spearmen_high"] = {
		culture = "other",
		class = "med",
		count = 160
	},

	["ltd_pol_sergeant_crossbowmen_lesser_poland"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["ltd_pol_sergeant_crossbowmen_silesia"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["ltd_pol_sergeant_spearmen"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["ltd_pol_sergeant_spearmen_silesia"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["ltd_pol_sergeant_swordsmen_high"] = {
		culture = "other",
		class = "med",
		count = 160
	},

	["ltd_pol_skirmishers_pomerania"] = {
		culture = "other",
		class = "med",
		count = 160
	},

	["ltd_pol_spearmen_pomerania"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["ltd_rus_boyar"] = {
		culture = "other",
		class = "med",
		count = 50
	},

	["ltd_rus_boyar_sons"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["ltd_rus_brigands"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["ltd_rus_chernye_klobuki"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["ltd_rus_dismounted_druzina"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["ltd_rus_druzina_cav"] = {
		culture = "other",
		class = "med",
		count = 80
	},

	["ltd_rus_guard_axemen"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["ltd_rus_heavy_spears"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["ltd_rus_hunters"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["ltd_rus_late_archers"] = {
		culture = "other",
		class = "rich",
		count = 120
	},

	["ltd_rus_militia_spear"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["ltd_rus_militia_spear_heavy"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["ltd_rus_senior_druzina"] = {
		culture = "other",
		class = "med",
		count = 80
	},

	["ltd_rus_vitjaz"] = {
		culture = "other",
		class = "rich",
		count = 80
	},

	["ltd_serb_archers"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["ltd_serb_auxiliary"] = {
		culture = "other",
		class = "med",
		count = 160
	},

	["ltd_serb_crossbowmen_late"] = {
		culture = "other",
		class = "med",
		count = 120
	},

	["ltd_serb_frontiersmen"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["ltd_serb_grandprince_bodyguard"] = {
		culture = "other",
		class = "med",
		count = 50
	},

	["ltd_serb_halberdier"] = {
		culture = "other",
		class = "rich",
		count = 160
	},

	["ltd_serb_king_bodyguard_high"] = {
		culture = "other",
		class = "rich",
		count = 50
	},

	["ltd_serb_knight_high"] = {
		culture = "other",
		class = "rich",
		count = 80
	},

	["ltd_serb_peasant_bowmen"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["ltd_serb_pronija_lancer"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["ltd_serb_skirmishers"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["ltd_serb_spearmen"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["ltd_serb_vlastela"] = {
		culture = "other",
		class = "rich",
		count = 80
	},

	["ltd_slav_aux_javelinmen"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["ltd_slav_spearmen"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["ltd_teuton_MAA_late"] = {
		culture = "other",
		class = "rich",
		count = 160
	},

	["ltd_teuton_bodyguard"] = {
		culture = "other",
		class = "med",
		count = 50
	},

	["ltd_teuton_crossbow_high"] = {
		culture = "other",
		class = "med",
		count = 120
	},

	["ltd_teuton_grandmaster_bodyguard_high"] = {
		culture = "other",
		class = "rich",
		count = 50
	},

	["ltd_teuton_grandmaster_bodyguard_late"] = {
		culture = "other",
		class = "med",
		count = 50
	},

	["ltd_teuton_halfbrothers"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["ltd_teuton_handgunner_late"] = {
		culture = "other",
		class = "rich",
		count = 120
	},

	["ltd_teuton_knight"] = {
		culture = "other",
		class = "med",
		count = 80
	},

	["ltd_teuton_knight_high"] = {
		culture = "other",
		class = "rich",
		count = 80
	},

	["ltd_teuton_knight_late"] = {
		culture = "other",
		class = "rich",
		count = 80
	},

	["ltd_teuton_livonian_crossbowmen"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["ltd_teuton_militia_spearmen"] = {
		culture = "other",
		class = "poor",
		count = 180
	},

	["ltd_teuton_mounted_crossbowmen"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["ltd_teuton_mounted_crossbowmen_high"] = {
		culture = "other",
		class = "med",
		count = 80
	},

	["ltd_teuton_mounted_sergeant_high"] = {
		culture = "other",
		class = "rich",
		count = 80
	},

	["ltd_teuton_pikemen_late"] = {
		culture = "other",
		class = "rich",
		count = 160
	},

	["ltd_teuton_prussian_archers"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["ltd_teuton_prussian_archers_high"] = {
		culture = "other",
		class = "med",
		count = 120
	},

	["ltd_teuton_sergeant"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["ltd_teuton_sergeant_high"] = {
		culture = "other",
		class = "med",
		count = 160
	},

	["ltd_teuton_sergeant_spearmen"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["ltd_teuton_sergeant_spearmen_high"] = {
		culture = "other",
		class = "med",
		count = 160
	},

	["ltd_teuton_swordbrethren"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["ltd_teuton_swordbrother_knight"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["ltd_trbznd_bodyguard"] = {
		culture = "other",
		class = "rich",
		count = 50
	},

	["ltd_trbznd_guard_archers"] = {
		culture = "other",
		class = "rich",
		count = 120
	},

	["ltd_trbznd_kavallarioi"] = {
		culture = "other",
		class = "med",
		count = 80
	},

	["ltd_trbznd_militia"] = {
		culture = "other",
		class = "poor",
		count = 180
	},

	["ltd_trbznd_peltastoi"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["ltd_trbznd_pronoia_lancers"] = {
		culture = "other",
		class = "rich",
		count = 80
	},

	["ltd_trbznd_scoutatoi"] = {
		culture = "other",
		class = "med",
		count = 160
	},

	["ltd_trbznd_scoutatoi_swordsmen"] = {
		culture = "other",
		class = "med",
		count = 160
	},

	["ltd_trbznd_toxotai"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["ltd_trbznd_trapezitae_mounted_javcav"] = {
		culture = "other",
		class = "med",
		count = 80
	},

	["ltd_trbznd_trebizond_archers"] = {
		culture = "other",
		class = "rich",
		count = 120
	},

	["ltd_turkish_archers"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["ltd_turkish_askari_cavalry"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["ltd_turkish_askari_nobles"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["ltd_turkish_cavalry"] = {
		culture = "other",
		class = "med",
		count = 80
	},

	["ltd_turkish_ghazi"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["ltd_turkish_ghulam_foot_guard"] = {
		culture = "other",
		class = "rich",
		count = 120
	},

	["ltd_turkish_junior_ghulam"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["ltd_turkish_rumi_archers"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["ltd_turkish_senior_ghulam"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["ltd_turkish_sultans_bodyguard"] = {
		culture = "other",
		class = "med",
		count = 50
	},

	["ltd_turkish_turcoman_raiders"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["ltd_turkish_veteran_ghazi"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["ltd_turkish_veteran_spearmen"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["ltd_varangian_guard"] = {
		culture = "other",
		class = "rich",
		count = 200
	},

	["ltd_vlach_heavy_warrior"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["ltd_vlach_spearmen"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["ltd_vlach_warrior"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["marshal_archers"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["marshal_billmen"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["marshal_castellans"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["marshal_maa"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["marshal_mounted_maa"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["marshal_mounted_serjeants"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["marshal_retinue"] = {
		culture = "other",
		class = "med",
		count = 50
	},

	["mk_catapult"] = {
		culture = "other",
		class = "poor",
		count = 40
	},

	["mk_mons_meg"] = {
		culture = "other",
		class = "med",
		count = 30
	},

	["mk_mortar"] = {
		culture = "other",
		class = "med",
		count = 40
	},

	["mon_armenian_archers"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["mon_armenian_footmen"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["mon_early_heavy_lancers"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["mon_early_horse_archers"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["mon_early_lancers"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["mon_generals_bodyguard"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["mon_georgian_auxiliaries"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["mon_handgunners"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["mon_heavy_horse_archer"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["mon_high_heavy_horse_archers"] = {
		culture = "other",
		class = "med",
		count = 80
	},

	["mon_high_heavy_lancers"] = {
		culture = "other",
		class = "rich",
		count = 80
	},

	["mon_high_persian_foot_archers"] = {
		culture = "other",
		class = "med",
		count = 120
	},

	["mon_high_persian_foot_soldiers"] = {
		culture = "other",
		class = "med",
		count = 160
	},

	["mon_keshiq"] = {
		culture = "other",
		class = "med",
		count = 50
	},

	["mon_kharash"] = {
		culture = "other",
		class = "poor",
		count = 200
	},

	["mon_nafta_throwers"] = {
		culture = "other",
		class = "med",
		count = 50
	},

	["mon_persian_cavalry"] = {
		culture = "other",
		class = "rich",
		count = 80
	},

	["mon_persian_foot_archers"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["mon_persian_foot_soldiers"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["mon_persian_knights"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["mon_qaraghul"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["mon_shahzagadan_guard"] = {
		culture = "other",
		class = "med",
		count = 50
	},

	["mon_tabhgur"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["mon_tammas"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["mon_tazik_levy"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["mon_turkic_archers"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["mon_turkic_horsemen"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["mont_retinue"] = {
		culture = "other",
		class = "med",
		count = 50
	},

	["montfort_archers"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["montfort_billmen"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["montfort_castellans"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["montfort_maa"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["montfort_mounted_maa"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["montfort_mounted_serjeants"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["montfort_retinue"] = {
		culture = "other",
		class = "med",
		count = 50
	},

	["mounted_armored_sergeants"] = {
		culture = "other",
		class = "rich",
		count = 80
	},

	["naples_crossbowmen"] = {
		culture = "other",
		class = "med",
		count = 120
	},

	["naples_high_bodyguard"] = {
		culture = "other",
		class = "med",
		count = 50
	},

	["naples_late_bodyguard"] = {
		culture = "other",
		class = "rich",
		count = 50
	},

	["naples_swordsmen"] = {
		culture = "other",
		class = "med",
		count = 160
	},

	["nor_t1_atgeir_bondir"] = {
		culture = "other",
		class = "poor",
		count = 180
	},

	["nor_t1_bodyguard"] = {
		culture = "other",
		class = "med",
		count = 50
	},

	["nor_t1_bondir"] = {
		culture = "other",
		class = "poor",
		count = 180
	},

	["nor_t1_bow_bondir"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["nor_t1_candle_men"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["nor_t1_crossbow_hirdmen"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["nor_t1_guest"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["nor_t1_guest_bodyguard"] = {
		culture = "other",
		class = "poor",
		count = 100
	},

	["nor_t1_hirdmen"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["nor_t1_knekts"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["nor_t1_orkney"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["nor_t1_riddere"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["nor_t1_spear_bondir"] = {
		culture = "other",
		class = "poor",
		count = 180
	},

	["nor_t1_tablemen"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["nor_t2_atgeir_bondir"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["nor_t2_bondir"] = {
		culture = "other",
		class = "med",
		count = 160
	},

	["nor_t2_bow_bondir"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["nor_t2_crossbow_bondir"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["nor_t2_crossbow_oppbud"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["nor_t2_crossbow_vaepnere"] = {
		culture = "other",
		class = "med",
		count = 120
	},

	["nor_t2_foot_riddere"] = {
		culture = "other",
		class = "rich",
		count = 160
	},

	["nor_t2_foot_riddere_bodyguard"] = {
		culture = "other",
		class = "rich",
		count = 100
	},

	["nor_t2_knekts"] = {
		culture = "other",
		class = "rich",
		count = 80
	},

	["nor_t2_landevern"] = {
		culture = "other",
		class = "med",
		count = 160
	},

	["nor_t2_mounted_riddere"] = {
		culture = "other",
		class = "rich",
		count = 80
	},

	["nor_t2_mounted_riddere_bodyguard"] = {
		culture = "other",
		class = "med",
		count = 50
	},

	["nor_t2_orkney"] = {
		culture = "other",
		class = "med",
		count = 160
	},

	["nor_t2_pike_oppbud"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["nor_t2_vaepnere"] = {
		culture = "other",
		class = "med",
		count = 160
	},

	["norfolk_archers"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["norfolk_billmen"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["norfolk_castellans"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["norfolk_maa"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["norfolk_mounted_maa"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["norfolk_mounted_serjeants"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["norfolk_retinue"] = {
		culture = "other",
		class = "med",
		count = 50
	},

	["normandy_castellans"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["normandy_crossbowmen"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["normandy_maa"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["normandy_mounted_maa"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["normandy_mounted_serjeants"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["normandy_retinue"] = {
		culture = "other",
		class = "med",
		count = 50
	},

	["normandy_spearmen"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["normandy_voulgier"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["peasent"] = {
		culture = "other",
		class = "poor",
		count = 200
	},

	["peasentarcher"] = {
		culture = "other",
		class = "poor",
		count = 180
	},

	["peasentdagger"] = {
		culture = "other",
		class = "poor",
		count = 200
	},

	["pembroke_archers"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["pembroke_billmen"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["pembroke_castellans"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["pembroke_maa"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["pembroke_mounted_maa"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["pembroke_mounted_serjeants"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["pembroke_retinue"] = {
		culture = "other",
		class = "med",
		count = 50
	},

	["perche_castellans"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["perche_crossbowmen"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["perche_maa"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["perche_mounted_maa"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["perche_mounted_serjeants"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["perche_retinue"] = {
		culture = "other",
		class = "med",
		count = 50
	},

	["perche_spearmen"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["perche_voulgier"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["retinue_longbowmen"] = {
		culture = "other",
		class = "rich",
		count = 120
	},

	["rus_berdiche_axemen"] = {
		culture = "other",
		class = "med",
		count = 160
	},

	["rus_boyar_dismount"] = {
		culture = "other",
		class = "med",
		count = 120
	},

	["rus_dismount_boyar_sons"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["rus_izgoi_scouts"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["rus_knyaz_guard_high"] = {
		culture = "other",
		class = "med",
		count = 50
	},

	["rus_levy_crossbows"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["rus_levy_crossbows_heavy"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["rus_muscovite_cav"] = {
		culture = "other",
		class = "rich",
		count = 80
	},

	["rus_novgorodspears"] = {
		culture = "other",
		class = "med",
		count = 160
	},

	["rus_suzdal_infi"] = {
		culture = "other",
		class = "med",
		count = 160
	},

	["rus_suzdal_spears"] = {
		culture = "other",
		class = "med",
		count = 160
	},

	["rus_westernized_infi"] = {
		culture = "other",
		class = "med",
		count = 160
	},

	["rus_westernized_spears"] = {
		culture = "other",
		class = "med",
		count = 160
	},

	["salisbury_archers"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["salisbury_billmen"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["salisbury_castellans"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["salisbury_maa"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["salisbury_mounted_maa"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["salisbury_mounted_serjeants"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["salisbury_retinue"] = {
		culture = "other",
		class = "med",
		count = 50
	},

	["saxony_early_bodyguard"] = {
		culture = "other",
		class = "med",
		count = 50
	},

	["saxony_high_bodyguard"] = {
		culture = "other",
		class = "med",
		count = 50
	},

	["saxony_knights_early"] = {
		culture = "other",
		class = "med",
		count = 80
	},

	["saxony_knights_high"] = {
		culture = "other",
		class = "rich",
		count = 80
	},

	["saxony_knights_late"] = {
		culture = "other",
		class = "rich",
		count = 80
	},

	["saxony_late_bodyguard"] = {
		culture = "other",
		class = "rich",
		count = 50
	},

	["saxony_late_maa"] = {
		culture = "other",
		class = "rich",
		count = 160
	},

	["scots_guard"] = {
		culture = "other",
		class = "rich",
		count = 120
	},

	["serb_gunners"] = {
		culture = "other",
		class = "rich",
		count = 120
	},

	["serb_heavy_infantry"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["serb_light_gunners"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["sicily_arsenlotti_crossbowmen"] = {
		culture = "other",
		class = "poor",
		count = 90
	},

	["sicily_arsenlotti_mariners"] = {
		culture = "other",
		class = "poor",
		count = 90
	},

	["sicily_arsenlotti_swordsmen"] = {
		culture = "other",
		class = "poor",
		count = 90
	},

	["sicily_concergius_guards"] = {
		culture = "other",
		class = "med",
		count = 160
	},

	["sicily_concergius_horsemen"] = {
		culture = "other",
		class = "rich",
		count = 80
	},

	["sicily_condo_maa"] = {
		culture = "other",
		class = "rich",
		count = 80
	},

	["sicily_early_bodyguard"] = {
		culture = "other",
		class = "med",
		count = 50
	},

	["sicily_early_crossbowmen"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["sicily_early_knights"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["sicily_german_knights"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["sicily_german_swordsmen"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["sicily_ghibelline_spearmen"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["sicily_guelph_maa"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["sicily_guelph_mounted_maa"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["sicily_high_bodyguard"] = {
		culture = "other",
		class = "med",
		count = 50
	},

	["sicily_high_knights"] = {
		culture = "other",
		class = "rich",
		count = 80
	},

	["sicily_italian_crossbow_militia"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["sicily_italian_jav_militia"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["sicily_italian_spear_militia"] = {
		culture = "other",
		class = "poor",
		count = 180
	},

	["sicily_italian_sword_militia"] = {
		culture = "other",
		class = "poor",
		count = 180
	},

	["sicily_late_archers"] = {
		culture = "other",
		class = "rich",
		count = 120
	},

	["sicily_late_bodyguard"] = {
		culture = "other",
		class = "rich",
		count = 50
	},

	["sicily_late_maa"] = {
		culture = "other",
		class = "rich",
		count = 160
	},

	["sicily_late_mounted_serjeants"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["sicily_late_pikemen"] = {
		culture = "other",
		class = "rich",
		count = 160
	},

	["sicily_late_spearmen"] = {
		culture = "other",
		class = "rich",
		count = 160
	},

	["sicily_light_gunners"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["sicily_mid_archers"] = {
		culture = "other",
		class = "med",
		count = 120
	},

	["sicily_mid_crossbowmen"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["sicily_mid_spearmen"] = {
		culture = "other",
		class = "med",
		count = 160
	},

	["sicily_mid_swordsmen"] = {
		culture = "other",
		class = "med",
		count = 160
	},

	["sicily_muslim_archers"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["sicily_muslim_horse_archers"] = {
		culture = "other",
		class = "med",
		count = 80
	},

	["swd_atgeir_bondir"] = {
		culture = "other",
		class = "poor",
		count = 180
	},

	["swd_axe_bondir"] = {
		culture = "other",
		class = "poor",
		count = 180
	},

	["swd_crossbowmen_early"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["swd_hirdmen_early"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["swd_huskarls_early"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["swd_knights_early"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["swd_longbows_early"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["swd_nobility_early"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["swd_royal_atgeir_hirdmen"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["swd_spear_bondir"] = {
		culture = "other",
		class = "poor",
		count = 180
	},

	["swisspikes"] = {
		culture = "other",
		class = "med",
		count = 160
	},

	["targon_spearmen"] = {
		culture = "other",
		class = "rich",
		count = 160
	},

	["trebuchet"] = {
		culture = "other",
		class = "poor",
		count = 50
	},

	["unterwaldhalberds"] = {
		culture = "other",
		class = "med",
		count = 160
	},

	["unterwaldswordsmen"] = {
		culture = "other",
		class = "med",
		count = 160
	},

	["urgrell_retinue"] = {
		culture = "other",
		class = "med",
		count = 50
	},

	["urihalberdiers"] = {
		culture = "other",
		class = "med",
		count = 160
	},

	["uriswordsmen"] = {
		culture = "other",
		class = "med",
		count = 160
	},

	["verman_retinue"] = {
		culture = "other",
		class = "med",
		count = 50
	},

	["vonerlach_knights"] = {
		culture = "other",
		class = "rich",
		count = 50
	},

	["voulge_militia"] = {
		culture = "other",
		class = "poor",
		count = 180
	},

	["winchester_archers"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["winchester_billmen"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["winchester_castellans"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["winchester_maa"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["winchester_mounted_maa"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["winchester_mounted_serjeants"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["winchester_retinue"] = {
		culture = "other",
		class = "med",
		count = 50
	},

	["zaehringermounted_sergeants"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["zaehringernoblefootmen"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["zaehringerschiltron"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["zaehringerspearmilitia"] = {
		culture = "other",
		class = "poor",
		count = 180
	},

	["zaehringerswordsmen"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["zsi_ashraaf_early"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["zsi_charkh_infantry"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["zsi_daylami_mercenaries"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["zsi_desert_cavalry"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["zsi_khwarazm_spearmen"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["zsi_khwarazmshahs_mercenary_guard"] = {
		culture = "other",
		class = "poor",
		count = 50
	},

	["zsi_qipchaq_mercenaries"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["zsi_qipchaq_mercenary_lancers"] = {
		culture = "other",
		class = "poor",
		count = 80
	},

	["zsi_rayat_skirmishers"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["zsi_rayat_spearmen"] = {
		culture = "other",
		class = "poor",
		count = 160
	},

	["zsi_tribal_archers"] = {
		culture = "other",
		class = "poor",
		count = 120
	},

	["zsi_turkoman_mercenary_lancers"] = {
		culture = "other",
		class = "med",
		count = 80
	},

	["zurichhalberdiers"] = {
		culture = "other",
		class = "med",
		count = 160
	},

	["zurichswordsmen"] = {
		culture = "other",
		class = "med",
		count = 160
	}
}